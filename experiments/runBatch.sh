#   Copyright 2017 Justs Zarins
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#!/bin/bash

#edit to match your batch system. 
#e.g. qsub for PBS
SUBMIT_COMMAND="qsub"


for d in $* ; do
	echo $d
	cd $d
	$SUBMIT_COMMAND submit.pbs
	cd .. 
done
