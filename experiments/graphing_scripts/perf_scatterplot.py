#   Copyright 2017 Justs Zarins
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#!/usr/bin/python
from __future__ import division #to force floating point division
import monitorParser
import sys
import numpy as np

#import matplotlib as mpl
#mpl.use("agg")
#mpl.pyplot.switch_backend('agg')

import matplotlib as mpl

import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd

import os, re

from adjustText import adjust_text

def remapNames(df, nameKey, names):
    for k,v in names.items():
        df.loc[df[nameKey] == k, nameKey] = v

def specifyNoise(df, nameKey, noiseKey, namesAndNoises):
    for k,v in namesAndNoises.items():
        df.loc[df[nameKey] == k, noiseKey] = v
    
def gotNoise(filename):
    f = open(filename, 'r')
    for l in f:
        if "noise" in l and not "no noise" in l:
            f.close()
            return True
        if "no noise" in l:
            f.close()
            return False

    f.close()
    return None

def getName(filename):
    out = ""
    f = open(filename, 'r')
   
    for l in f:
        re1 = re.search("^(.+?),", l)
        re2 = re.search("manager: (.+)", l)

        if re2:
            out = out + "\n" + re2.group(1)
        elif re1:
            out = out + re1.group(1)
       

    f.close()
    return out



def get_comparison_df():
    dataSet = []
    outputFolder = "/output/"
    metaFilename = "/meta/description.txt"
    finalIters = []
    tileSize = int(sys.argv[1])

    #take a list of folders as input
    for folder in sys.argv[2:]:
        print("reading", folder)
        gotN = gotNoise(folder + metaFilename)
        name = getName(folder + metaFilename)
        print("found name", name)
        for i in [tileSize]:
            subfolder = folder + outputFolder + str(i)
            files = monitorParser.getFilenames(subfolder)

            for f in files:
                data = {}
                monitorParser.parseUnifiedDumpSamples(f, data)
                
                #try multiple functions to get time and iteration data
                #monitorParser.parseDumpSamples(f, data)
                #if len(data["times"]) == 0:
                #    data = {}
                #    monitorParser.parseSync(f, data)

                #monitorParser.parseLastSample(f, data) 

                finalIters.append([x/data["times"][0] for x in  data["samples"][-1]])
                avgIters = np.mean(data["samples"])
                avgTime = np.mean(data["times"])
                iterRate = avgIters / avgTime

                maxDiffPerc = ((np.max(data["samples"]) / np.min(data["samples"])) - 1.0) * 100

                try:
                    spread = max(data["samples"][-1]) - min(data["samples"][-1])
                except:
                    print(folder)
                    print(os.path.basename(f))
                    print(data["samples"])
                    sys.exit(0)
                #spread = 1.0*spread / avgIters
                #spread = 1.0*avgIters / spread

                dataSet.append({"name": name, "folder": folder, "file": os.path.basename(f),  "spread": spread, "iterRate": iterRate, "avgIters": avgIters, "avgTime": avgTime, "dim": i, "hostname": data["hostname"], "maxDiffPerc": maxDiffPerc, "noise": gotN})



    df = pd.DataFrame(dataSet)

    return df


if __name__ == "__main__":
    df = get_comparison_df()
    noisyXlim = None
    noisyYlim = None
    normXlim = None
    normYlim = None
    
    for dryRun in [True, False]:
        for noisy in [True, False]:
            title = 'No added noise'
            if noisy:
                title = 'With added noise'
                
            #aesthetics
            sns.set_context("paper")
            sns.set_style("ticks", {'axes.grid': True})


            data2 = []
            
            for i in df.folder.unique():
                subdf = df[df.folder == i]
                name = subdf.name.iloc[0]
                managed = False
                if '\n' in name:
                    managed = True
                iterRateMedian = np.median(subdf.iterRate)
                irUpErr = np.abs(np.percentile(subdf.iterRate, 25) - iterRateMedian)
                irDownErr = np.abs(np.percentile(subdf.iterRate, 75) - iterRateMedian)
                spreadMedian = np.median(subdf.spread)
                sUpErr = np.abs(np.percentile(subdf.spread, 25) - spreadMedian)
                sDownErr = np.abs(np.percentile(subdf.spread, 75) - spreadMedian)
                gotN = subdf.iloc[0]['noise']
                data2.append({'folder': i, 'name': name, 'iterRateMed': iterRateMedian, 'spreadMed': spreadMedian, 'gotNoise': gotN,
                    'irUpErr': irUpErr, 'irDownErr': irDownErr, 'sUpErr': sUpErr, 'sDownErr': sDownErr, 'managed': managed})


            df2 = pd.DataFrame(data2)
            df2 = df2[df2.gotNoise == noisy]

            ax = plt.gca()
            for m in [True, False]:
                color = (1.0, 109/255.0, 182/255.0)
                size = None
                marker = 'o'
                label = "Normal"
                if m == True:
                    color = (0.0, 109/255.0, 219/255.0)
                    marker = 's'
                    label = "Load balanced"

                xerr = np.array([[ df2[df2.managed == m]['irUpErr'].values,
                                   df2[df2.managed == m]['irDownErr'].values ]])
                yerr = np.array([[ df2[df2.managed == m]['sUpErr'].values,
                                   df2[df2.managed == m]['sDownErr'].values ]])
                print(xerr)
                print(yerr)

                if len(df2[df2.managed == m]) > 0:
                    df2[df2.managed == m].plot.scatter(x='iterRateMed', y='spreadMed', xerr=xerr, yerr=yerr, color=color, s=size, ax=ax, marker=marker, label=label, fontsize=12)

           

            def label_point(x, y, val, ax, texts):
                a = pd.concat({'x': x, 'y': y, 'val': val}, axis=1)
                for i, point in a.iterrows():
                    t = ax.text(point['x'], point['y'], str(point['val']), fontsize=12)
                    texts.append(t)

            texts = []
            label_point(df2.iterRateMed, df2.spreadMed, df2.name, ax, texts)

            adjust_text(texts, force_text=0.1, arrowprops=dict(arrowstyle="-", color='k', lw=0.5))

            plt.xlabel("Iteration rate", fontsize=12)
            plt.ylabel("Update spread", fontsize=12)
            plt.legend(frameon=True, framealpha=1, loc=2, fontsize=12)
            
            if noisy:
                noisyXlim = ax.get_xlim()
                noisyYlim = ax.get_ylim()
            else:
                normXlim = ax.get_xlim()
                normYlim = ax.get_ylim()
            
            if dryRun:
                plt.close()
                continue
            else:
                #try to have similar axes limits between plots
                ax.set_xlim([min(normXlim[0], noisyXlim[0]), max(normXlim[1], noisyXlim[1])])
                ax.set_ylim([min(normYlim[0], noisyYlim[0]), max(normYlim[1], noisyYlim[1])])


            plt.gcf().set_size_inches(25, 15)
            noise_text = "no_noise_"
            if noisy:
                noise_text = "with_noise_"
            plt.savefig(noise_text + "landscape.pdf", format="pdf")
            plt.close()

