#   Copyright 2017 Justs Zarins
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#!/usr/bin/python
from __future__ import division #to force floating point division
import perf_scatterplot as ps

import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd

def getdf2(df):
    data2 = []
    
    for i in df.folder.unique():
        subdf = df[df.folder == i]
        name = subdf.name.iloc[0]
        managed = False
        if '\n' in name:
            managed = True
        iterRateMedian = np.median(subdf.iterRate)
        irUpErr = np.percentile(subdf.iterRate, 25)
        irDownErr = np.percentile(subdf.iterRate, 75)
        spreadMedian = np.median(subdf.spread)
        sUpErr = np.percentile(subdf.spread, 25)
        sDownErr = np.percentile(subdf.spread, 75)
        timeMedian = np.median(subdf.avgTime)
        timeUpErr = np.percentile(subdf.avgTime, 25)
        timeDownErr = np.percentile(subdf.avgTime, 75)


        gotN = subdf.iloc[0]['noise']
        data2.append({'folder': i, 'name': name, 'iterRateMed': iterRateMedian, 'spreadMed': spreadMedian, 'gotNoise': gotN,
            'irUpErr': irUpErr, 'irDownErr': irDownErr, 'sUpErr': sUpErr, 'sDownErr': sDownErr, 'timeMed': timeMedian, 'timeUpErr': timeUpErr, 'timeDownErr': timeDownErr, 'managed': managed})


    df2 = pd.DataFrame(data2)
    return df2


def writeNoiseTable(filename, df):
    with open(filename, 'w') as f:
        f.write("Run type\ttime increase when noise is added\n")

        for name in df.name.unique():
            subdf = df[df.name == name]
            timeRatio = subdf[subdf.gotNoise == "With added noise"].timeMed.iloc[0] / subdf[subdf.gotNoise == "Normal"].timeMed.iloc[0]
            timePercent = round((timeRatio - 1) * 100)
            
            f.write(name + "\t" + str(timePercent) + "%\n")


if __name__ == "__main__":
    df = ps.get_comparison_df()
    df["noise"].replace(to_replace=[False], value="Normal", inplace=True)
    df["noise"].replace(to_replace=[True], value="With added noise", inplace=True)

    df2 = getdf2(df)
    print(df2)

    #aesthetics
    #sns.set(font_scale=1.0)
    sns.set_context("paper")
    sns.set_style("ticks", {'axes.grid': True})
    colors = [(1.0, 109/255.0, 182/255.0), (0.0, 109/255.0, 219/255.0)]
    col_pal = sns.color_palette(colors)

    fs=18
    
    ax = sns.boxplot(x="name", y="avgTime", hue="noise", palette=col_pal, fliersize=3, data=df)
    ax.tick_params(labelsize=fs)
    plt.xlabel("Synchronisation type", fontsize=fs)
    plt.ylabel("Time to solution (s)", fontsize=fs)
    plt.legend(frameon=True, framealpha=1, fontsize=fs)

    plt.gcf().set_size_inches(25, 6.5)
    plt.savefig("boxplot.pdf", format='pdf', dpi=300)
    plt.close()

    #plt.show()

    writeNoiseTable("table.txt", df2)
