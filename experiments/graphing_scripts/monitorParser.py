#   Copyright 2017 Justs Zarins
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#!/usr/bin/python
from __future__ import division #to force floating point division
import sys, re
import numpy as np
from operator import itemgetter
import matplotlib.pyplot as plt
import seaborn as sns
import pandas as pd
from scipy import stats
import os
import pickle

##################################################
#             Data parsing functions             #
##################################################
def parseSamples(filename, data, sampleTypes):
    f = open(filename, "r") 

    readingSamples = False
    readingInit = False
    initR = 0

    samples = [[] for i in range(sampleTypes)]
    times = []

    sampleToTake = 0

    for line in f:
        samplingStopRE = re.search("finished printing sampling data\.", line) #Printing global grid to file...
        initRE = re.search("Initial residual array:", line)

        if initRE:
            readingInit = True
        
        if readingInit:
            iR = map(float, re.findall("\d*\.?\d+", line))    
            if len(iR) > 0:
                initR = iR
                readingInit = False
            
    

        if samplingStopRE:
            readingSamples = False

        if readingSamples:
            #sample = [int(s) for s in line.split() if s.isdigit()]
            sample = map(float, re.findall("\d*\.?\d+", line))
            samples[sampleToTake].append(sample)
            sampleToTake = (sampleToTake + 1) % sampleTypes
        else:
            samplingStartRE = re.search("printing sampling data\.\.\.", line)
            if samplingStartRE: 
                readingSamples = True

        timeRE  = re.search("Time\(s\) for thread \d+ = (\d+\.\d+)", line)
        if timeRE:
            times.append(float(timeRE.group(1)))


        
                


    data["samples"] = np.array(samples[0])
    if (sampleTypes == 2):
        data["samplesR"] = np.array(samples[1])
    data["initR"] = initR
    data["nThreads"] = len(samples[0][0])
    data["times"] = times
    f.close()


def parseDumpSamples(filename, data):
    f = open(filename, "r")
    samples = []
    times = []
    hostname = "none"

    for line in f:
        itersRE = re.search("Iters for work item \d+ = (\d+)", line)
        timeRE  = re.search("Time\(s\) for thread \d+ = (\d+\.\d+)", line)
        placementRE = re.search("Thread = (\d+), core = (\d+), initial workID = (\d+)", line)
        hostnameRE = re.search("rank 0 is on (nid\d+)", line)
        
        if itersRE:
            samples.append(int(itersRE.group(1)))
        elif timeRE:
            times.append(float(timeRE.group(1)))
        elif hostnameRE:
            hostname = str(hostnameRE.group(1))
        elif placementRE:
            thr = int(placementRE.group(1))
            core = int(placementRE.group(2))
            if thr != core:
                print("mismatch [thread, core]", thr, core)

    data["samples"] = np.array([samples])
    data["nThreads"] = len(samples)
    data["times"] = times
    data["hostname"] = hostname
    f.close()
    

def parseUnifiedDumpSamples(filename, data):
    f = open(filename, "r")
    samples = []
    times = []
    hostname = "none"

    for line in f:
        itersRE = re.search("Iters for work item \d+ = (\d+)", line)
        hostnameRE = re.search("rank 0 is on (.+)", line)
        timeRE = re.search("Calculation time: (\d+\.\d+)", line)
        stopRE = re.search("printing sampling data...", line)
        
        if itersRE:
            samples.append(int(itersRE.group(1)))
        elif timeRE:
            tm = float(timeRE.group(1))
            if tm > 3600:
                print("file:", filename)
                print("parsed time", tm, "which seems too high. skipping.")
            else:
                times.append(tm)
        elif hostnameRE:
            hostname = str(hostnameRE.group(1))
        elif stopRE:
            break

    data["samples"] = np.array([samples])
    data["times"] = np.array([times])
    data["nThreads"] = len(samples)
    data["hostname"] = hostname
    f.close()


def parseLastSample(filename, data):
    f = open(filename, "r")
    lines = f.readlines()

    #for l in reversed(lines):
    sample = map(int, re.findall("\d+", lines[-4]))
    data["samples"] = np.array([sample])

    f.close()
        



def parseSync(filename, data):
    #Calculation time: 7.102593
    #Final Iteration: 50000 | Residual: 0.000174

    f = open(filename, "r")
    iters = 50000 
    time = -1
    hostname = "none"

    for line in f:
        itersRE = re.search("Final Iteration: (\d+)", line)
        timeRE = re.search("Calculation time: (\d+\.\d+)", line)
        hostnameRE = re.search("rank 0 is on (nid\d+)", line)

        if itersRE:
            iters = int(itersRE.group(1))
        elif timeRE:
            time = float(timeRE.group(1))
        elif hostnameRE:
            hostname = str(hostnameRE.group(1))

    data["samples"] = np.array([[iters]])
    data["times"] = np.array([[time]])
    data["hostname"] = hostname
    f.close()


def setupStyle(data):
    sns.set_style("whitegrid")
    sns.set_context("notebook", font_scale=1.5, rc={"lines.linewidth": 2})
    sns.set_palette(sns.color_palette("husl", data["nThreads"]), n_colors=data["nThreads"])

def setupPosterStyle():
    sns.set_context("poster", font_scale=1.5, rc={"lines.linewidth": 2})

    ax = plt.gca()
    #ax.get_xaxis().get_major_formatter().set_useOffset(True)
    #ax.get_yaxis().get_major_formatter().set_useOffset(True)

    plt.xlim(0,35000)
    plt.ylim(0,300000)

    ax.set_xticklabels(range(0, 36, 5))
    ax.set_yticklabels(range(0, 301, 50))

    plt.xlabel("Time (s)")
    plt.ylabel("Completed iterations (x1000)")

    ax.legend_.remove()
    plt.tight_layout()

def interactive_legend(ax=None):
    if ax is None:
        ax = plt.gca()
    if ax.legend_ is None:
        ax.legend()

    return InteractiveLegend(ax.legend_)

class InteractiveLegend(object):
    def __init__(self, legend):
        self.legend = legend
        self.fig = legend.axes.figure

        self.lookup_artist, self.lookup_handle = self._build_lookups(legend)
        self._setup_connections()

        self.update()

    def _setup_connections(self):
        for artist in self.legend.texts + self.legend.legendHandles:
            artist.set_picker(10) # 10 points tolerance

        self.fig.canvas.mpl_connect('pick_event', self.on_pick)
        self.fig.canvas.mpl_connect('button_press_event', self.on_click)

    def _build_lookups(self, legend):
        labels = [t.get_text() for t in legend.texts]
        handles = legend.legendHandles
        label2handle = dict(zip(labels, handles))
        handle2text = dict(zip(handles, legend.texts))

        lookup_artist = {}
        lookup_handle = {}
        for artist in legend.axes.get_children():
            if artist.get_label() in labels:
                handle = label2handle[artist.get_label()]
                lookup_handle[artist] = handle
                lookup_artist[handle] = artist
                lookup_artist[handle2text[handle]] = artist

        lookup_handle.update(zip(handles, handles))
        lookup_handle.update(zip(legend.texts, handles))

        return lookup_artist, lookup_handle

    def on_pick(self, event):
        handle = event.artist
        if handle in self.lookup_artist:
            artist = self.lookup_artist[handle]
            artist.set_visible(not artist.get_visible())
            self.update()

    def on_click(self, event):
        if event.button == 3:
            visible = False
        elif event.button == 2:
            visible = True
        else:
            return

        for artist in self.lookup_artist.values():
            artist.set_visible(visible)
        self.update()

    def update(self):
        for artist in self.lookup_artist.values():
            handle = self.lookup_handle[artist]
            if artist.get_visible():
                handle.set_visible(True)
            else:
                handle.set_visible(False)
        self.fig.canvas.draw()

    def show(self):
        plt.show()


def plotSubs(data):
    fig = plt.figure(1)
    fig.text(0.5, 0.04, 'Time (s)', ha='center', va='center')
    fig.text(0.06, 0.5, 'Iterations', ha='center', va='center', rotation='vertical')
    ax1 = plt.subplot(211)
    ax1.set_title("Raw")
    plotRaw(data, ax1)
    #plt.setp(ax.lines, color="gray")
    ax2 = plt.subplot(212, sharex=ax1)
    ax2.set_title("Spread")
    plotSpread(data, ax2)



def plotRaw(data, ax):
    x = [i * data["samplingRate"] for i in range(0, len(data["samples"][:,0]))]
    for i in range(0,data["nThreads"]):
            ax.plot(x, data["samples"][:,i], label=i)
    plt.ylabel("Iterations")
    plt.xlabel("Time (s)")




def plotGradients(data, step):
    gradients = []
    for i in range(step, len(data["samples"]), step):
        gradients.append((data["samples"][i] - data["samples"][i-step]) / (step * data["samplingRate"]))
        gradients = np.array(gradients)
        x = [(i+1) * step * data["samplingRate"] for i in range(0, len(gradients[:,0]))]
        for i in range(0,data["nThreads"]):
            plt.plot(x, gradients[:,i], label=i)

    plt.ylabel("Iteration rate (1/s)")
    plt.xlabel("Time (s)")

def plotGradientSpread(data, step):
    gradients = []
    for i in range(step, len(data["samples"]), step):
        gradients.append((data["samples"][i] - data["samples"][i-step]) / step)
    spread = [max(x) - min(x) for x in gradients]
    lines = plt.plot(spread)

def subtractMin(row):
    m = min(row)
    return [x - m for x in row]    

#normalize data by minimum value in each sample
def plotRawFloored(data):
    datar = np.apply_along_axis(subtractMin, 1, data["samples"])
    lines = plt.plot(datar)
    plt.legend(lines, range(0,data["nThreads"]))
    
def plotMinMax(data):
    plt.plot(np.argmin(data["samples"], 1), 'o', label="min")
    plt.plot(np.argmax(data["samples"], 1), 'o', label="max")
    plt.legend()

def plotAvgs(data):
    datar = np.apply_along_axis(np.mean, 1, data["samples"])
    lines = plt.plot(datar, '.')

    #add fit line
    x = range(0, len(datar))
    slope, intercept, r_value, p_value, std_err = stats.linregress(x,datar)
    
    plt.plot(x, [(slope * i + intercept) for i in x])
    print("R^2 is", r_value)

def maxMinusMin(row):
    return max(row) - min(row)

def plotSpread(data, ax):
    datar = np.apply_along_axis(maxMinusMin, 1, data["samples"])
    ax.plot(datar)

def printDiffs(data):
    datar = np.apply_along_axis(maxMinusMin, 1, data["samples"])
    #print "spread of iterations: min, max, mean"
    print(min(datar), max(datar), np.mean(datar))

def applyToDir(directory, func):
    filenames = os.listdir(directory)
    for f in filenames:
        data = {}
        parseFile(f, data)
        print(f)
        func(data)

def plotMinMaxHist(data):
    bins = range(0,data["nThreads"])
    minCounts = np.bincount(np.argmin(data["samples"], 1), minlength=data["nThreads"])
    maxCounts = np.bincount(np.argmax(data["samples"], 1), minlength=data["nThreads"])

    #width = 1.0
    #ax = plt.axes()
    #ax.set_xticks((width / 2))
    #ax.set_xticklabels(bins)

    plt.bar(bins, minCounts, label="mins", width=1, color='r', alpha=0.5)
    plt.bar(bins, maxCounts, label="maxs", width=1, color='g', alpha=0.5)
    plt.xticks([x+0.5 for x in bins], bins)
    plt.legend()

#def printMinMaxThreads(data):
#    minCounts = np.bincount(np.argmin(data["samples"], 1))
#    maxCounts = np.bincount(np.argmax(data["samples"], 1))
#    print np.argmax(minCounts), np.argmax(maxCounts)

def getRVal(data):
    datar = np.apply_along_axis(np.mean, 1, data["samples"])
    x = range(0, len(datar))
    slope, intercept, r_value, p_value, std_err = stats.linregress(x,datar)
    return r_value

def getTimeStats(data):
    return np.mean(data["times"]), stats.sem(data["times"])


def getMostMinID(data):
    minCounts = np.bincount(np.argmin(data["samples"], 1))
    return np.argmax(minCounts)

def getMostMaxID(data):
    maxCounts = np.bincount(np.argmax(data["samples"], 1))
    return np.argmax(maxCounts)


def getSpreadStats(data):
    datar = np.apply_along_axis(maxMinusMin, 1, data["samples"])
    sErr = -1
    if len(datar) > 1:
        sErr = stats.sem(datar)
    return min(datar), max(datar), np.mean(datar), sErr 

def getCoords(me, npy):
    return [int(me / npy), me % npy]

def getNeighbours(me, npx, npy):
    coords = getCoords(me, npy)
    neighbours = []
    if (coords[0] > 0):
        neighbours.append(me - npy)
    else:
        neighbours.append(me)
    
    if (coords[0] < npx - 1):
        neighbours.append(me + npy)
    else:
        neighbours.append(me)
    
    if (coords[1] < npy - 1):
        neighbours.append(me + 1)
    else:
        neighbours.append(me)
    
    if (coords[1] > 0):
        neighbours.append(me - 1)
    else:
        neighbours.append(me)

    return neighbours


#iters is a list of iterations at a common point in time; 
#npx and npy are the dimensions of the problem domain
npx = 8
npy = 6
def getNeighbourSpread(iters):
    spreads = []
    for i in range(0, len(iters)):
        neigh = getNeighbours(i, npx, npy)
        spread = 0
        for n in neigh:
            spread = spread + abs(iters[i] - iters[n])
        spreads.append(spread / 4)
    return np.max(spreads)

def getOverallNeighbourSpread(data):
    datar = np.apply_along_axis(getNeighbourSpread, 1, data["samples"])
    return np.mean(datar)

def makeOrderHeatmap(threadOrders, nThreads):
    df = pd.DataFrame(threadOrders)
    rows = range(0, nThreads)
    cols = [str(i) + "th" for i in rows]

    #create empty heatmap
    heatMap = pd.DataFrame(index=rows, columns=cols)
    heatMap = heatMap.fillna(0)

    for col in cols:
        counts = df[col].value_counts()
        for k,v in counts.iteritems():
            heatMap[col][k] = v

    return heatMap


def getThreadFinalOrder(data):
    finalIters = data["samples"][-1]
    withIDs = zip(finalIters, range(0, data["nThreads"]+1)) #???
    withIDs.sort(reverse=True)
    #withIDs = zip(withIDs, range(0, data["nThreads"]+1))
    threadFinalOrder = {}
    for i in range(0, len(withIDs)):
        threadFinalOrder[str(i) + "th"] = withIDs[i][1]
        
    return threadFinalOrder


def makeClusterHeatmap(clusters, nThreads):
    rows = range(0, nThreads)
    cols = ["fast", "slow"]

    heatMap = pd.DataFrame(index=rows, columns=cols)
    heatMap = heatMap.fillna(0)

    for c in clusters:
        for i in c["slow"]:
            heatMap["slow"][i] = heatMap["slow"][i] + 1
        for i in c["fast"]:
            heatMap["fast"][i] = heatMap["fast"][i] + 1

    return heatMap


def getThreadClusters(data):
    finalIters = data["samples"][-1]
    withIDs = zip(finalIters, range(0, data["nThreads"]+1)) #???
    withIDs.sort(reverse=True)

    #assume that there is one large drop in iterations, i.e. 2 clusters
    #seems to be valid on ARCHER
    maxDrop = 0
    maxID = 0
    for i in range(0, len(withIDs)-1):
        drop = withIDs[i][0] - withIDs[i+1][0]
        if drop > maxDrop:
            maxDrop = drop
            maxID = i

    clusters = {"fast":[], "slow":[]}
    for i in range(0, maxID+1):
        clusters["fast"].append(withIDs[i][1])

    for i in range(maxID+1, len(withIDs)):
        clusters["slow"].append(withIDs[i][1])

    return clusters


def getFinalIters(data):
    finalIters = data["samples"][-1]
    withIDs = []
    for i in range(0, len(finalIters)):
        withIDs.append({"core": i, "finalIters": finalIters[i], "filename":data["filename"]})

    return withIDs
        

def printFinalIterHeatmap(fi, npx, npy):
    print("npx", npx)
    print("npy", npy)
    for i in range(0, npx*npy):
        print(np.mean(fi[fi["core"] == i]["finalIters"]))
    for i in range(0, npx*npy):
        print(stats.sem(fi[fi["core"] == i]["finalIters"]))


def convertFinalItersToRates(fi, df, cores):
    sets = int(len(fi) / cores)
    out = pd.DataFrame()
    for s in range(0, sets):
        a = fi[s*cores:(s+1)*cores].copy()
    
        if (np.max(a["finalIters"]) - np.min(a["finalIters"])) != df["maxSpread"][s]:
            print("conversion failed")
            return pd.DataFrame() #empty data frame

        a["finalIters"] = a["finalIters"].apply(lambda x: x/df["meanTime"][s])
        out = pd.concat([out, a])

    return out


def getSpreadPercentage(fi, df, cores):
    sets = int(len(fi) / cores)
    out = []
    for s in range(0, sets):
        a = fi[s*cores:(s+1)*cores]
    
        if (np.max(a["finalIters"]) - np.min(a["finalIters"])) != df["maxSpread"][s]:
            print("conversion failed")
            return [] 

        out.append(df["maxSpread"][s] / np.mean(a["finalIters"]))

    return out


###Functions for residuals###
def calcRelativeResidual(current, initial):
    denum = np.sqrt(np.sum(initial))
    if denum == 0:
        #print "WARNING: denuminator was 0 when calculating residual"
        return np.sqrt(np.sum(current))
    return np.sqrt(np.sum(current)) / denum

def getGlobalResiduals(data):
    res = []
    for r in data["samplesR"]:
        res.append(calcRelativeResidual(r, data["initR"]))
    return res

def getLocalResiduals(data):
    res = [[] for i in range(data["nThreads"])]
    for r in data["samplesR"]:
        for i in range(data["nThreads"]):
            res[i].append(calcRelativeResidual(r[i], data["initR"][i]))
    return res

def plotLocalResiduals(data):
    res = getLocalResiduals(data)
    for i in range(data["nThreads"]):
        plt.plot(res[i], label=i)
    ax = plt.gca()
    ax.set_yscale("log")
    plt.legend()

def plotGlobalResiduals(data):
    res = getGlobalResiduals(data)
    plt.plot(res)
    ax = plt.gca()
    ax.set_yscale("log")
    

def getFilenames(directory):
    return [os.path.abspath(directory + "/" + f) for f in os.listdir(directory)]

##################################################
#     Multiple experiment result coalescing      #
##################################################
def multipleExperiments(directory):
    allTrials = []
    experiments = [os.path.abspath(directory + "/" + d) for d in os.listdir(directory)]
    for e in experiments:
        print("processing", e)
        allTrials = allTrials + roundupExperiment(e)

    return pd.DataFrame(allTrials)

def roundupExperiment(directory, dump):
    trials = []
    threadOrders = []
    clusters = []
    finalIters = []
    coreID = os.path.basename(directory)
    #int(re.search(".*(\d+).*", os.path.basename(directory)).group(1))
    nThreads = -1

    trialNames = getFilenames(directory)
    for tn in trialNames:
        #print tn
        trial = {}
        trial["coreID"] = coreID
        trial["filename"] = os.path.basename(tn)
        data = {"filename":trial["filename"]}
        if dump:
            parseDumpSamples(tn, data)
        else:
            parseSamples(tn,data)
        if nThreads == -1:
            nThreads = data["nThreads"]
        
        trial["finalIters"] = data["samples"][-1]

        finalIters = finalIters + getFinalIters(data)
        threadOrders.append(getThreadFinalOrder(data))
        clusters.append(getThreadClusters(data))
        trial["rVal"]       = getRVal(data)
        trial["mostMinID"]  = getMostMinID(data)
        trial["mostMaxID"]  = getMostMaxID(data)
        trial["minSpread"], trial["maxSpread"], trial["meanSpread"], trial["sErrSpread"] = getSpreadStats(data)
        trial["finalNeighSpread"] = getNeighbourSpread(data["samples"][-1])
        trial["overallNeighSpread"] = getOverallNeighbourSpread(data)
        trial["meanTime"], trial["sErrTime"] = getTimeStats(data)
        trials.append(trial)

    threadOrderHeatMapData = makeOrderHeatmap(threadOrders, nThreads)
    clusterHeatMapData = makeClusterHeatmap(clusters, nThreads)

    return trials, threadOrderHeatMapData, clusterHeatMapData, pd.DataFrame(finalIters)
        
            
#PICKLE! -> save on raw data processing time
def pickleData(data, name):
    dumpfile = open(name, "w")
    pickle.dump(data, dumpfile)
    dumpfile.close()

def unpickleData(name):
    inDump = open(name, "r")
    data = pickle.load(inDump)
    inDump.close()
    return data

def makeHeatMapData(df):
    label1 = "monitorCore"
    label2 = "core"
    label3 = "mostMaxIDcounts"

    summary = [{"core":c, "data":df[df["coreID"] == c]["mostMaxID"].value_counts()} for c in ["core"+str(i) for i in range(12,24)]]
    newDf = []
    for s in summary:
        for core,count in s["data"].iteritems():
            newDf.append({label1: s["core"], label2: core, label3: count})
    
    newDf = pd.DataFrame(newDf)
    newDf = newDf.pivot(label1, label2, label3)
    newDf = newDf.fillna(0) #replace missing values with 0s
    return newDf
        

    
if __name__ == "__main__":
    data = {} 
    df = None
    t = None
    hm = None
    cl = None
    fi = None
    rates = None


    if len(sys.argv) > 1:
            if sys.argv[1] == "-s":
                    filename = sys.argv[2]
                    parseSamples(filename, data, 2)
                    #parseDumpSamples(filename, data)
                    data["samplingRate"] = 1 #100000 / 1000000
                    #setupStyle(data)
                    plotSubs(data)
                    #plotRaw(data)
                    #plotLocalResiduals(data)
                    #plotGlobalResiduals(data)
                    #plotGradients(data, 3)
                    #setupPosterStyle()
                    #plotRawFloored(data)
                    #plotGradientSpread(data, 150)
                    #plotMinMax(data)
                    #plotAvgs(data)
                    #plotSpread(data)
                    #plotMinMaxHist(data)

                    #plt.legend(loc="upper left", bbox_to_anchor=(1,1))
                    #interactive_legend().show()

                    plt.show()
            elif sys.argv[1] == "-sd":
                    filename = sys.argv[2]
                    parseDumpSamples(filename, data)
                    setupStyle(data)

                    rates = [data["samples"][0][i]/data["times"][i] for i in range(0,data["nThreads"])]
                    plt.plot(rates, 'o')

                    plt.show()
            elif sys.argv[1] == "-e":
                    folder = sys.argv[2]
                    t, hm, cl, fi = roundupExperiment(folder, False)
                    df = pd.DataFrame(t)
                    print("time =", np.mean(df["meanTime"]), "+-", stats.sem(df["meanTime"]))
                    print("final_spread =", np.mean(df["maxSpread"]), "+-", stats.sem(df["maxSpread"]))
                    print("final_neigh_spread =", np.mean(df["finalNeighSpread"]), "+-", stats.sem(df["finalNeighSpread"]))
                    print("overall_neigh_spread =", np.mean(df["overallNeighSpread"]), "+-", stats.sem(df["overallNeighSpread"]))
            elif sys.argv[1] == "-ed":
                    folder = sys.argv[2]
                    npx = int(sys.argv[3])
                    npy = int(sys.argv[4])
                    t, hm, cl, fi = roundupExperiment(folder, True)
                    df = pd.DataFrame(t)
                    print("time =", np.mean(df["meanTime"]), "+-", stats.sem(df["meanTime"]))
                    print("final_spread =", np.mean(df["maxSpread"]), "+-", stats.sem(df["maxSpread"]))
                    print("final_neigh_spread =", np.mean(df["finalNeighSpread"]), "+-", stats.sem(df["finalNeighSpread"]))
                    print("overall_neigh_spread =", np.mean(df["overallNeighSpread"]), "+-", stats.sem(df["overallNeighSpread"]))

                    data["nThreads"] = npx * npy
                    setupStyle(data)
                    rates = convertFinalItersToRates(fi, df, npx * npy)
                    sns.boxplot(x="core", y="finalIters", data=rates)
                    plt.ylabel("Iteration rate (1/s)")
                    plt.show()
                    
                    #printFinalIterHeatmap(fi, 4, 6)
                    
    else:
            
            #df = multipleExperiments(sys.argv[1])
            #pickleData(df, "noMonitorData.pickle")

            #df = unpickleData("noMonitorData.pickle")
            #newDf = makeHeatMapData(df)
            #ax = sns.heatmap(newDf)
            #ax.invert_yaxis()
            #plt.show()

            #df = multipleExperiments("./movingMonitors/byExperiment/")
            #newDf = makeHeatMapData(df)
            #ax = sns.heatmap(newDf)
            #ax.invert_yaxis()
            plt.show()
















