#   Copyright 2017 Justs Zarins
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#!/usr/bin/python
import sys
import generate

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print "Please provide number of cores as an argument."
        sys.exit()
    
    num_cores = int(sys.argv[1])
    sides = generate.getSides(num_cores)
    npx = sides[0]
    npy = sides[1]
    

    noise = -1
    if npx % 2 == 0:
        noise = (npx / 2 - 1) * npy
    else:
        noise = (npx / 2) * npy

    #print "num_cores =", num_cores
    #print "npx =", npx
    #print "npy =", npy
    print "noise placement =", noise

