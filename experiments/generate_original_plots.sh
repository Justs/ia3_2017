#   Copyright 2017 Justs Zarins
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

python3 graphing_scripts/boxplots.py 300 data/time_to_solution/cirrus/*
mv boxplot.pdf cirrus_boxplot.pdf
mv table.txt cirrus_table.txt
python3 graphing_scripts/boxplots.py 300 data/time_to_solution/archer/*
mv boxplot.pdf archer_boxplot.pdf
mv table.txt archer_table.txt


python3 graphing_scripts/perf_scatterplot.py 300 data/landscape/cirrus/*
mv no_noise_landscape.pdf cirrus_no_noise_landscape.pdf
mv with_noise_landscape.pdf cirrus_with_noise_landscape.pdf

python3 graphing_scripts/perf_scatterplot.py 300 data/landscape/archer/*
mv no_noise_landscape.pdf archer_no_noise_landscape.pdf
mv with_noise_landscape.pdf archer_with_noise_landscape.pdf
