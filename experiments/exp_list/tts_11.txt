###General settings

jacobiType 4

npx 6

npy 6

myN 300

tolerance 0.0001

maxIters -5000

resCalcFreq 1000

enableNoise 0

noiseSize 10000

noiseFreq 200

xsplit 2

ysplit 2

###Load balancer settings

managerType 4

managingFreqTime 1000.001

nPairs 6

lowerThresh 2

upperThresh 6

maxStaleness 30

splits 2

crossSocketFreq 300

numCores 36
