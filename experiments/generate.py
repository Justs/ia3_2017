#   Copyright 2017 Justs Zarins
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#!/usr/bin/python
import sys
import os
import shutil
import fileinput
import re


def findParam(filename, pattern):
    with open(filename, 'r') as f:
        for line in f:
            res = re.search(pattern, line)
            if res:
                return(res.group(1))

def findAndReplace(filename, old, new):
    tempname = filename + ".tmp"
    f = open(filename, 'r')
    tmp = open(tempname, "w")
    for line in f:
        tmp.write(new if old in line else line)
    f.close()
    tmp.close()
    os.rename(tempname, filename)

def generateDescription(dest, config):
    str_to_write = ""
    jacobi = findParam(config, "jacobiType (\d+)")
    xsplit = findParam(config, "xsplit (\d+)")
    ysplit = findParam(config, "ysplit (\d+)")
    maxStaleness = findParam(config, "maxStaleness (\d+)")
    if jacobi == '0':
        str_to_write = str_to_write + "sync, "
    elif jacobi == '2':
        str_to_write = str_to_write + "ssync(" + maxStaleness + "), "
    elif jacobi == '4':
        product = int(xsplit) * int(ysplit)
        str_to_write = str_to_write + "async(" + str(product) + "), "

    noise = findParam(config, "enableNoise (\d+)")
    if noise == "1":
        str_to_write = str_to_write + "noise\n"
    elif noise == "0":
        str_to_write = str_to_write + "no noise\n"

    mfreq = findParam(config, "managingFreqTime (\d+\.\d+)")
    splits = findParam(config, "splits (\d+)")
    cross = findParam(config, "crossSocketFreq (-?\d+)")
    if float(mfreq) < 1000: #more than 1000 would essentially be load balancing turned off
        str_to_write = str_to_write + "manager: "
        if splits == '1':
            str_to_write = str_to_write + "joint(" + mfreq + ")"
        elif splits == '2' and cross == '-1':
            str_to_write = str_to_write + "split(" + mfreq + ")"
        elif splits == '2' and cross != '-1':
            str_to_write = str_to_write + "hybrid(" + mfreq + ", " + cross + ")"

    with open(dest, 'w') as f:
        f.write(str_to_write) 


def getSides(num_cores):
    if num_cores % 2 != 0:
        print "number of cores must be a multiple of 2"
        sys.exit()

    npx = 1
    npy = num_cores
    for i in range(1, num_cores+1):
        if num_cores % i == 0 and i <= num_cores / i:
            npx = i
            npy = num_cores / i

    return (npx, npy)


if __name__ == "__main__":
   #get base dir
   #read in list of experiments to generate from file
   #make dirs, copy over relevant files and edit settings as per input list

    argc = len(sys.argv)
    if argc != 3:
        print "usage: python2 generate.py num_cores path_to_executable"
        sys.exit()

    
    num_cores = sys.argv[1]
    exec_path = sys.argv[2]
    submit_file = "./submit.pbs"
    exp_list_dir = "./exp_list"
    
    sides = getSides(int(num_cores))
    npx = sides[0]
    npy = sides[1]

    #check inputs
    if not os.path.isabs(exec_path):
        print "Executable path should be absolute but got: " + exec_path + "\nExiting."
        sys.exit()

    for filename in os.listdir(exp_list_dir):
        print "processing " + filename
        basename = os.path.splitext(filename)[0]
        if not os.path.isdir(basename):
            os.makedirs(basename)
            config_filename = basename + "/config.txt" 
            sub_filename = basename + "/submit.pbs"
            
            shutil.copy(exp_list_dir + "/" + filename, config_filename)
            findAndReplace(config_filename, "npx 6", "npx " + str(npx) + "\n")
            findAndReplace(config_filename, "npy 6", "npy " + str(npy) + "\n")
            findAndReplace(config_filename, "numCores 36", "numCores " + str(num_cores) + "\n")

            shutil.copy(submit_file, sub_filename)
            dim = findParam(basename + "/config.txt", "myN (\d+)")
            findAndReplace(sub_filename, "APP=", "APP=" + exec_path + "\n")
            findAndReplace(sub_filename, "CONFIG=", "CONFIG=config.txt\n")
            findAndReplace(sub_filename, "DIM=", "DIM=" + dim + "\n")


            os.makedirs(basename + "/meta")
            generateDescription(basename + "/meta/description.txt", config_filename)


        

