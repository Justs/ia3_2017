MF=	Makefile
CC=	mpicc
CFLAGS=  -Iinc/ -O2 -std=c99 -fopenmp -Wno-implicit-function-declaration
LFLAGS= -lm 

EXESRC= src/exe/jacobi_main.c src/exe/jacobi_main_threads.c
COMSRC=	$(wildcard src/common/*.c) 
COMOBJ= $(patsubst src/common/%.c, %.o, $(COMSRC))
EXES= 	$(EXESRC:src/exe/%.c=%)
INC=	$(wildcard inc/*.h)

#############################


all: $(EXES) $(INC) $(MF) 

accumulator.o : src/common/accumulator.c $(INC) $(MF) | objFolder
	#$(CC) -O0 -std=gnu99 -fopenmp -Iinc/ -c $< -o obj/$@
	$(CC) -O0 -Iinc/ -c $< -o obj/$@

#create object folder if it doesn't exist
%.o : src/*/%.c $(INC) $(MF) | objFolder
	$(CC) $(CFLAGS) -c $< -o obj/$@

objFolder:
	@mkdir -p obj/

#rule for making an executable
% :  $(COMOBJ) %.o
	$(CC) $(CFLAGS) -o $@ $(addprefix obj/, $^) $(LFLAGS)

clean:
	rm -f $(EXES) obj/* 
cleanout:
	rm -f *.sgf.* parallelOut*.dat *.pgm


debug:
	@echo $(EXESRC)
	@echo $(COMSRC)
	@echo $(COMOBJ)
	@echo $(EXES)	
	@echo $(INC)
