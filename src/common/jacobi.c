/*   Copyright 2017 Justs Zarins

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "precision.h"
#include "jacobi.h"
#include "ioutils.h"
#include "pgmio.h"
#include GSL


RealNumber calcLocalResidual(int rows, int cols, RealNumber* grid) {
	RealNumber normR = 0.0;
	RealNumber temp = -1;
	for (int i = 1; i < cols-1; i++) {
		for (int j = 1; j < rows-1; j++) {
			//if (threadID == 0)
			//		printf("grid: %d %d %f\n", i, j, grids[threadID][idx(rows, i, j)]);
			temp = 4 * grid[idx(rows, i, j)] - 
				   grid[idx(rows, i+1, j)] - 
				   grid[idx(rows, i-1, j)] - 
				   grid[idx(rows, i, j+1)] - 
				   grid[idx(rows, i, j-1)];
			normR += temp * temp;
		}
	}


	return normR;
}


void getHalos(int rows, int cols, int me, int* neigh, RealNumber** grids) {
	//get from LEFT
	if (neigh[0] > -1) {
		for (int j = 1; j < rows-1; j++)
			grids[me][idx(rows, 0, j)] = grids[neigh[0]][idx(rows, cols-2, j)];
	}
	
	//get from RIGHT
	if (neigh[1] > -1) {
		for (int j = 1; j < rows-1; j++)
			grids[me][idx(rows, cols-1, j)] = grids[neigh[1]][idx(rows, 1, j)];
	}
	
	
	//get from UP
	if (neigh[2] > -1) {
		for (int i = 1; i < cols-1; i++)
			grids[me][idx(rows, i, rows-1)] = grids[neigh[2]][idx(rows, i, 1)];
	}
	
	//get from DOWN
	if (neigh[3] > -1) {
		for (int i = 1; i < cols-1; i++)
			grids[me][idx(rows, i, 0)] = grids[neigh[3]][idx(rows, i, rows-2)];
	}
}


void updateGrid(int rows, int cols, RealNumber* newGrid, RealNumber* grid) {
	for (int i = 1; i < cols+1; i++) {
		for (int j = 1; j < rows+1; j++) {
			newGrid[idx(rows+2, i, j)] = 0.25 * (grid[idx(rows+2, i-1, j)] + 
					  		     grid[idx(rows+2, i+1, j)] + 
  	   						     grid[idx(rows+2, i, j-1)] + 
							     grid[idx(rows+2, i, j+1)]);
		}
	}
}


void updateGridPartially(int rows, int cols, RealNumber* newGrid, RealNumber* grid, RealNumber skipPercentage, const gsl_rng* r) {
	if (skipPercentage == 0) {
		updateGrid(rows, cols, newGrid, grid);
	} else {
		/*int stride = (int) (1.0 / skipPercentage);
		//maybe add random offset

		for (int i = 1; i < cols+1; i++) {
			for (int j = 1; j < rows+1; j++) {
				if (((i - 1) * (rows) + j - 1) % stride != 0) {
					newGrid[idx(rows+2, i, j)] = 0.25 * (grid[idx(rows+2, i-1, j)] + 
									     grid[idx(rows+2, i+1, j)] + 
									     grid[idx(rows+2, i, j-1)] + 
									     grid[idx(rows+2, i, j+1)]);
				}
			}
		}*/

		//int colsToSkip = (int) (rows * skipPercentage);
		

		/*for (int i = 1; i < cols+1; i++) {
			for (int j = 1; j < rows+1; j++) {
				if (gsl_rng_uniform(r) > skipPercentage) {
					newGrid[idx(rows+2, i, j)] = 0.25 * (grid[idx(rows+2, i-1, j)] + 
									     grid[idx(rows+2, i+1, j)] + 
									     grid[idx(rows+2, i, j-1)] + 
									     grid[idx(rows+2, i, j+1)]);
				}
			}
		}*/



		int nSkip = (int) (rows * skipPercentage + 1);
		int colsToSkip[nSkip];
		for (int i = 0; i < nSkip; i++) {
			colsToSkip[i] = i*(rows/nSkip) + 1 + gsl_rng_uniform_int(r, rows/nSkip);
			printf("%d\n", colsToSkip[i]);
		} 
		printf("----\n");
		int skipCounter = 0;
		

		for (int i = 1; i < cols+1; i++) {
			if (i != colsToSkip[skipCounter]) {
				for (int j = 1; j < rows+1; j++) {
					newGrid[idx(rows+2, i, j)] = 0.25 * (grid[idx(rows+2, i-1, j)] + 
									     grid[idx(rows+2, i+1, j)] + 
									     grid[idx(rows+2, i, j-1)] + 
									     grid[idx(rows+2, i, j+1)]);
				}
				
			} else {
				skipCounter++;
			}
		}

	}
}


void copybackGrid(int rows, int cols, RealNumber* newGrid, RealNumber* grid) {
	for (int i = 1; i < cols+1; i++) {
		for (int j = 1; j < rows+1; j++) 
			grid[idx(rows+2, i, j)] = newGrid[idx(rows+2, i, j)];
	}
}


void printGridsToFile(int rowsG, int colsG, RealNumber** grids, int rows, int cols, int n, int npy) {
	#ifdef DONT_PRINT_GRID
	printf("NOT printing global grid to file...\n");
	return;
	#endif

	printf("Printing global grid to file...\n");
	//make a contiguous buffer out of the individual buffers
	RealNumber* buf = malloc(rowsG * colsG * sizeof(RealNumber));
	int* coords = malloc(2 * sizeof(int));
	int colsOffset = 0;
	int rowsOffset = 0;
	
	for (int g = 0; g < n; g++) {
		getCoords(g, npy, coords);
		colsOffset = coords[0] * (cols - 2);
		rowsOffset = coords[1] * (rows - 2);
		for (int i = 1; i < cols-1; i++) {
			for (int j = 1; j < rows-1; j++) {
				buf[idx(rowsG, i-1 + colsOffset, j-1 + rowsOffset)] = grids[g][idx(rows, i, j)];
			}
		}
	}
	
	pgmwrite("ompOutput.pgm", buf, colsG, rowsG);
	free(buf);
	free(coords);
	printf("Printing complete.\n");
}


void getCoords(int n, int npy, int* coords) {
	coords[0] = n / npy;
	coords[1] = n % npy;
}


void getNeighbours(int n, int npx, int npy, int* coords, int* neighbours) {
	if (coords[0] > 0)       neighbours[0] = n - npy; else neighbours[0] = -1;
	if (coords[0] < npx - 1) neighbours[1] = n + npy; else neighbours[1] = -1;
	if (coords[1] < npy - 1) neighbours[2] = n + 1;   else neighbours[2] = -1;
	if (coords[1] > 0)       neighbours[3] = n - 1;   else neighbours[3] = -1;
}
