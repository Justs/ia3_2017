/*   Copyright 2017 Justs Zarins

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "precision.h"
#include "jacobi_omp.h"
#include "jacobi.h"
#include "ioutils.h"
#include "pgmio.h"
#include GSL
#include <omp.h>

void getGlobalNormRSync(int rows, int cols, RealNumber* grid, RealNumber* globalNormR, RealNumber normaliser) {
	RealNumber residual = calcLocalResidual(rows+2, cols+2, grid);
	#pragma omp single
		*globalNormR = 0.0;
	#pragma omp critical(normR_accum)
		*globalNormR += residual;
	#pragma omp barrier
	#pragma omp single
		*globalNormR = sqrt(*globalNormR) / normaliser;
}

void subsplitInit(int rows, int cols, int colsG, RealNumber** grids, RealNumber initVal, RealNumber scale, const gsl_rng* r, WorkItem** workItems, int nWIS) {
	for (int i = 0; i < nWIS; i++) {
			initialiseGridOMP(rows, cols, colsG, grids[workItems[i]->id], workItems[i]->coords, initVal, scale, r); 
	}
}


RealNumber initialiseGridOMP(int rows, int cols, int colsG, RealNumber* grid, int* coords, RealNumber initVal, RealNumber scale, const gsl_rng* r) {
	printf("Thread %d initialising.\n", omp_get_thread_num());
	//initialise the whole grid to a single value, halos included
	for (int i = 0; i < cols+2; i++) {
		for (int j = 0; j < rows+2; j++) {
			grid[idx(rows+2, i, j)] = initVal;
		}
	}

	for (int i = 1; i < cols+1; i++) {
		for (int j = 1; j < rows+1; j++) {
			if (r == NULL) {
				//leave grid unchanged
				//grid[idx(rows+2, i, j)] = initVal;
			} else {
				//make initial guess of x random, halos excluded
				#pragma omp critical(grid_init)
				{
					grid[idx(rows+2, i, j)] = gsl_rng_uniform(r);
				}
			}
		}
	}

	RealNumber x = 0.0;
	RealNumber temp = 0.0;
	RealNumber normB = 0.0;

	//boundary conditions, create a bump at the bottom of the box (on last row)
	if (coords[1] == 0) {
		for (int i = 1; i < cols+1; i++) {
			x = (i+coords[0]*cols)/(colsG+1.0); 
			temp = exp(-scale*(0.5-x)*(0.5-x));
			//temp = scale * gsl_rng_uniform(r) ;
			grid[idx(rows+2, i, 0)] = grid[idx(rows+2, i, 0)] + temp;
			normB += temp*temp;
		}	
	}

	
	/*if (coords[0] == 0) {
		for (int j = 1; j < rows+1; j++) {
			temp = scale * gsl_rng_uniform(r) ;
			grid[idx(rows+2, 0, j)] = grid[idx(rows+2, 0, j)] + temp;
			normB += temp*temp;
		}	
	}
	if (coords[0] == colsG-1) {
		for (int j = 1; j < rows+1; j++) {
			temp = scale * gsl_rng_uniform(r) ;
			grid[idx(rows+2, 0, j)] = grid[idx(rows+2, 0, j)] + temp;
			normB += temp*temp;
		}	
	}*/

	printf("Thread %d done initialising.\n", omp_get_thread_num());
	return normB;
}

RealNumber setupResidualsSync(int rows, int cols, RealNumber* grid, RealNumber* globalNormR, RealNumber* initGlobalNormR) {
	RealNumber gridResidual = calcLocalResidual(rows+2, cols+2, grid);
	#pragma omp atomic
		*globalNormR += gridResidual;
	#pragma omp barrier

	#pragma omp single
	{
		*initGlobalNormR = sqrt(*globalNormR);
		*globalNormR = sqrt(*globalNormR) / *initGlobalNormR;
	}

	return gridResidual;
}
