/*   Copyright 2017 Justs Zarins

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>
#include <sched.h>
#include "precision.h"
#include "ioutils.h"
#include "jacobi.h"
#include "jacobi_omp.h"
#include "pgmio.h"
#include "settings.h"


int omp_jacobi_sync_loose(int npx, int npy, int myN, RealNumber tolerance, int resCalcFreq, WorkItem** workItems) {
	printf("Using loose synchronous Jacobi implementation.\n");
/*===Initialise variables===*/
	RealNumber scale = 100; // Larger means less diffuse input;
	
	int rowsG, colsG;	//G stands for global
	int nThreads = npx * npy;
	
	int rows = myN; //local  rows
	int cols = myN; //local  cols
	rowsG = rows * npy; //global rows
	colsG = cols * npx; //global cols
	
	//arrays for intermediate reconstruction calculations, created with size +1 in each direction for halos
	RealNumber** grids = malloc(nThreads * sizeof(RealNumber*));
	RealNumber** newGrids = malloc(nThreads * sizeof(RealNumber*));
	RealNumber initVal = 1.0;
	RealNumber globalNormR = 0.0;
	RealNumber initGlobalNormR = 0.0;
	RealNumber globalAvg = -1;

	RealNumber** upBufs    = malloc(nThreads * sizeof(RealNumber*));
	RealNumber** downBufs  = malloc(nThreads * sizeof(RealNumber*));
	RealNumber** leftBufs  = malloc(nThreads * sizeof(RealNumber*));
	RealNumber** rightBufs = malloc(nThreads * sizeof(RealNumber*));

	double duration;

/*===Start main parallel region===*/
	#pragma omp parallel num_threads(nThreads) default(none) shared(grids, newGrids, rows, cols, npx, npy, colsG, scale, nThreads, rowsG, initGlobalNormR, globalNormR, initVal, tolerance, resCalcFreq, duration, upBufs, downBufs, leftBufs, rightBufs, workItems) 
	{
		int threadID = omp_get_thread_num();
		grids[threadID] = malloc((rows + 2) * (cols + 2) * sizeof(RealNumber));
		newGrids[threadID] = malloc((rows + 2) * (cols + 2) * sizeof(RealNumber));

		//1 more spot added to hold a flag
		upBufs[threadID]    = calloc((cols+1), sizeof(RealNumber));
		downBufs[threadID]  = calloc((cols+1), sizeof(RealNumber));
		leftBufs[threadID]  = calloc((rows+1), sizeof(RealNumber));
		rightBufs[threadID] = calloc((rows+1), sizeof(RealNumber));

		WorkItem* wi = malloc(sizeof(WorkItem));
                wi->id = threadID;
		wi->pe = sched_getcpu();
		wi->updates = 0;
		wi->residual = -1;
		
		getCoords(threadID, npy, wi->coords);
		getNeighbours(threadID, npx, npy, wi->coords, wi->neigh);
             	
		workItems[wi->id] = wi;

		initialiseGridOMP(rows, cols, colsG, grids[threadID], wi->coords, initVal, scale, NULL);
		setupResidualsSync(rows, cols, grids[threadID], &globalNormR, &initGlobalNormR);

		#pragma omp single 
		{
			printf("Running on %d threads.\nGlobal problem size: %d rows by %d columns.\nLocal problem size: %d rows by %d columns.\n ", nThreads, rowsG, colsG, rows, cols);
			printf("sqrt((b-Ax)**2) = %f\n", initGlobalNormR);
			printf("Initial norm r = %f\n", globalNormR);
		}
	
		int iter;
/*===Iterate Jacobi===*/

		#pragma omp single	
			duration = omp_get_wtime();

		//for (iter = 0; globalNormR > tolerance; iter++) {
		for (iter = 0; iter < MAX_ITERS; iter++) {
			int* neigh = workItems[threadID]->neigh;

			//printf("thread %d dispatching halos on iter %d\n", threadID, iter);
			//dispatch halos
			if (neigh[0] > -1) {
				#pragma omp flush
				while (leftBufs[threadID][rows] != 0) {
					#pragma omp flush
				}
					
				for (int j = 1; j < rows+1; j++)
					leftBufs[threadID][j - 1] = grids[threadID][idx(rows+2, 1, j)];
				leftBufs[threadID][rows] = 1;
				#pragma omp flush
			}
			
			if (neigh[1] > -1) {
				#pragma omp flush
				while (rightBufs[threadID][rows] != 0) {
					#pragma omp flush
				}
				for (int j = 1; j < rows+1; j++)
					rightBufs[threadID][j - 1] = grids[threadID][idx(rows+2, cols, j)];
				rightBufs[threadID][rows] = 1;
				#pragma omp flush
			}
			
			
			if (neigh[2] > -1) {
				#pragma omp flush
				while (upBufs[threadID][cols] != 0) {
					#pragma omp flush
				}
				for (int i = 1; i < cols+1; i++)
					upBufs[threadID][i - 1] = grids[threadID][idx(rows+2, i, rows)];
				upBufs[threadID][cols] = 1;
				#pragma omp flush
			}
			
			if (neigh[3] > -1) {
				#pragma omp flush
				while (downBufs[threadID][cols] != 0) {
					#pragma omp flush
				}
				for (int i = 1; i < cols+1; i++)
					downBufs[threadID][i - 1] = grids[threadID][idx(rows+2, i, 1)];
				downBufs[threadID][cols] = 1;
				#pragma omp flush
			}

			//get halos
			//printf("thread %d getting halos on iter %d\n", threadID, iter);

			//get from LEFT
			if (neigh[0] > -1) {
				#pragma omp flush
				while (rightBufs[neigh[0]][rows] != 1) {
					#pragma omp flush
				}
				for (int j = 1; j < rows+1; j++)
					grids[threadID][idx(rows+2, 0, j)] = rightBufs[neigh[0]][j-1];
				rightBufs[neigh[0]][rows] = 0;
				#pragma omp flush
			}
			
			//get from RIGHT
			if (neigh[1] > -1) {
				#pragma omp flush
				while (leftBufs[neigh[1]][rows] != 1) {
					#pragma omp flush
				}
				for (int j = 1; j < rows+1; j++)
					grids[threadID][idx(rows+2, cols+1, j)] = leftBufs[neigh[1]][j-1];
				leftBufs[neigh[1]][rows] = 0;
				#pragma omp flush
			}
			
			
			//get from UP
			if (neigh[2] > -1) {
				#pragma omp flush
				while (downBufs[neigh[2]][cols] != 1) {
					#pragma omp flush
				}
				for (int i = 1; i < cols+1; i++)
					grids[threadID][idx(rows+2, i, rows+1)] = downBufs[neigh[2]][i-1];
				downBufs[neigh[2]][cols] = 0;
				#pragma omp flush
			}
			
			//get from DOWN
			if (neigh[3] > -1) {
				#pragma omp flush
				while (upBufs[neigh[3]][cols] != 1) {
					#pragma omp flush
				}
				for (int i = 1; i < cols+1; i++)
					grids[threadID][idx(rows+2, i, 0)] = upBufs[neigh[3]][i-1];
				upBufs[neigh[3]][cols] = 0;
				#pragma omp flush
			}

			
			updateGrid(rows, cols, newGrids[threadID], grids[threadID]);
			copybackGrid(rows, cols, newGrids[threadID], grids[threadID]);
		
			//calculate how close the existing answer is close to the real answer
			if (iter % resCalcFreq == 0) {
				getGlobalNormRSync(rows, cols, grids[threadID], &globalNormR, initGlobalNormR);
				#pragma omp single nowait
					printf("Iteration: %i | Residual: %f\n", iter, globalNormR);
			}

			(workItems[threadID]->updates)++;
		}

		#pragma omp single
		{
			duration = omp_get_wtime() - duration;
		}
	}


/*===Printing and cleanup===*/
	printExperimentOutput(duration, workItems, nThreads, grids, nThreads, cols, rows, initGlobalNormR);
	printGridsToFile(rowsG, colsG, grids, rows+2, cols+2, nThreads, npy);
	//should free grids and newGrids...
	
	return 0;
}
