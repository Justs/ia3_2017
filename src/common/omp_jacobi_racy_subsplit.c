/*   Copyright 2017 Justs Zarins

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>
#include <sched.h>
#include <unistd.h>
#include "precision.h"
#include "ioutils.h"
#include "jacobi.h"
#include "jacobi_omp.h"
#include "thread_manager.h"
#include "utils.h"
#include "pgmio.h"
#include "settings.h"
#include <mpi.h>
#include "accumulator.h"
#include GSL

#define SEED 918237


/*
*  npx, npy number of PE's in x and y directions
*  myN local problem size (local domains are square)
*
*/

int omp_jacobi_racy_subsplit(int xsplit, int ysplit, int npx, int npy, int myN, RealNumber tolerance, int maxIters, int resCalcFreq, Manager* manager) {
	printf("Using racy Jacobi with subsplits implementation.\n");
/*===Initialise variables===*/
	WorkItem** workItems = manager->ctx->workItems;
	
	RealNumber scale = 100; // Larger means less diffuse input;
	int rowsG, colsG; //G stands for global
	int split = xsplit * ysplit;
	int extra = 10; //number of slots for extra work items
	int nThreads = npx * npy;
	
	int rows = myN / ysplit; //local  rows
	int cols = myN / xsplit; //local  cols
	rowsG = rows * npy * ysplit; //global rows
	colsG = cols * npx * xsplit; //global cols
	
	//arrays for intermediate reconstruction calculations, created with size +1 in each direction for halos
	RealNumber** grids = malloc(nThreads * split * sizeof(RealNumber*));
	RealNumber** newGrids = malloc(nThreads * split * sizeof(RealNumber*));
	RealNumber initVal = 1.0;
	RealNumber globalNormR = 0.0;
	RealNumber initGlobalNormR = 0.0;
	RealNumber globalAvg = -1;
	RealNumber goalVal = -1;

	int* coreIters = calloc(nThreads, sizeof(int));
	int* threadIters = calloc(nThreads, sizeof(int));
	double duration;
	double lastTime = 0;
	


	const gsl_rng* r = gsl_rng_alloc(gsl_rng_mt19937);
	gsl_rng_set(r, SEED); //set seed;
	

	volatile int managingThread = 0;
	volatile int shouldContinue = 1;

/*===Start main parallel region===*/
	#pragma omp parallel num_threads(nThreads) default(none) shared(grids, newGrids, rows, cols, npx, npy, colsG, scale, nThreads, rowsG, globalNormR, initVal, tolerance, maxIters, resCalcFreq, goalVal, lastTime, coreIters, threadIters, r, initGlobalNormR, split, extra, xsplit, ysplit, workItems, managingThread, shouldContinue, duration, manager)
	{
		#ifdef INIT_BY_THREAD_ID
		int tid = omp_get_thread_num();
		#else
		int tid = sched_getcpu();
		#endif

		#pragma omp critical(printingIters)
			printf("Init: Thread = %d, core = %d.\n", tid, sched_getcpu());

		WorkItem** myWIs = malloc((split + extra) * sizeof(WorkItem*));
		for (int i = 0; i < split + extra; i++) {
			myWIs[i] = NULL;
		}

		//allocate main working arrays distributedly so they end up in memory banks near the CPUs that use them

		for (int i = 0; i < xsplit; i++) {
			for (int j = 0; j < ysplit; j++) {
				int wid = j + i * npy * ysplit + tid * ysplit + (int) (tid / npy) * npy * ysplit * (xsplit - 1);
				printf("tid = %d. wid = %d.\n", tid, wid);
				WorkItem* wi = malloc(sizeof(WorkItem));
				wi->id = wid;
				wi->pe = tid;
				wi->updates = 0;
				wi->residual = -1;

				omp_init_lock(&(wi->lock));
				getCoords(wid, npy * ysplit, wi->coords);
				getNeighbours(wid, npx * xsplit, npy * ysplit, wi->coords, wi->neigh);

				workItems[wid] = wi;

				myWIs[i*ysplit + j] = wi;

				grids[wid] = malloc((rows + 2) * (cols + 2) * sizeof(RealNumber));
				newGrids[wid] = malloc((rows + 2) * (cols + 2) * sizeof(RealNumber));
			}
		}
		
		

		RealNumber tempGlobalNormR;
		
		//execute initialisation in a set order to be repeatable
		#pragma omp for ordered schedule(static,1)
		for(int t = 0; t < nThreads; t++) {
			#pragma omp ordered
			subsplitInit(rows, cols, colsG, grids, initVal, scale, NULL, myWIs, split);
		}

		#pragma omp barrier
		for (int i = 0; i < split; i++) {
			getHalos(rows+2, cols+2, myWIs[i]->id, myWIs[i]->neigh, grids); //need to get halos here in order to have correct residual calculations (if using random initialisation)
		}
		#pragma omp barrier

		for (int i = 0; i < split; i++)
			myWIs[i]->residual = calcLocalResidual(rows+2, cols+2, grids[myWIs[i]->id]);
		#pragma omp flush

		#pragma omp barrier

		#pragma omp single
		{
			printf("Global problem size: %d rows by %d columns.\nLocal problem size: %d rows by %d columns.\n ", rowsG, colsG, rows, cols);
			printf("Initial residual array:\n");
			for (int i = 0; i < nThreads * split; i++) 
				printf("%.14f ", workItems[i]->residual);
			printf("\n");
			
			goalVal = tolerance; // * tolerance * globalNormB * globalNormB;
			for (int i = 0; i < nThreads * split; i++)
				globalNormR += workItems[i]->residual;//globalNormB * globalNormB;
			printf("pre-sqrt: %f\n", globalNormR);
			globalNormR = sqrt(globalNormR); // / globalNormB;
			initGlobalNormR = globalNormR;
			printf("initGlobalNormR=%f\n", initGlobalNormR);
			globalNormR /= initGlobalNormR;
			printf("Initial norm r = %f\n", globalNormR);
		}


/*===Iterate Jacobi===*/
		double start = omp_get_wtime();
		if (tid == managingThread)
			lastTime = omp_get_wtime();
		int c = split;
		while (shouldContinue == 1) {

				#ifdef MANAGE
				if (manager->ctx->dirty[tid]) {
					/*for (int i = 0; i < split + extra; i++) {
						myWIs[i] = NULL;
					}*/
					
					c = 0;
		
					for (int i = 0; i < nThreads * split; i++) {
						if (workItems[i]->pe == tid) {
							myWIs[c] = workItems[i];
							c++;
						}
					}
					#ifdef MANAGER_OUTPUT
						printf("Thread %d new number of work items: %d. \n", tid, c);
					#endif
					manager->ctx->dirty[tid] = 0;
				//	break;
				}
				#endif //MANAGE


			for (int w = 0; w < c; w++) {
				WorkItem* wi = myWIs[w];
		
				int workID = wi->id;


				omp_set_lock(&(wi->lock));

				getHalos(rows+2, cols+2, workID, wi->neigh, grids);

				updateGrid(rows, cols, newGrids[workID], grids[workID]);
				copybackGrid(rows, cols, newGrids[workID], grids[workID]);
			
				//calculate how close the existing answer is to the real answer
				if (wi->updates % resCalcFreq == 0) {
					wi->residual = calcLocalResidual(rows+2, cols+2, grids[workID]);
					#pragma omp flush
				}

				#pragma omp single nowait 
				{
					if (wi->updates % resCalcFreq == 0 && w == 0) {
						RealNumber tempGlobalNormR = 0;
						for (int i = 0; i < nThreads * split; i++)
							tempGlobalNormR += workItems[i]->residual;
						globalNormR = sqrt(tempGlobalNormR) / initGlobalNormR;		
                        #pragma omp flush(workItems)
						printf("Iteration: %i | Residual ~ %.14f\n", wi->updates, globalNormR);
						
                        if (tolerance >= 0 && globalNormR <= tolerance) {
                            shouldContinue = 0;
                        }
					}
				}

				
				(wi->updates)++;
                //coreIters[sched_getcpu()]++;
				threadIters[tid]++;

				omp_unset_lock(&(wi->lock));
				
				#ifdef MANAGE
				//#pragma omp single nowait
				if (tid == managingThread)
				{
					if (omp_get_wtime() - lastTime > manager->managingFreqTime) {
						manager->manage(manager->ctx);
						lastTime = omp_get_wtime();
						managingThread = (managingThread + 1) % nThreads;
					}
				}
				#endif //MANAGE
			} //end work item loop

			if (maxIters >= 0 && threadIters[tid] >= maxIters * split) {
				shouldContinue = 0;
			}
		} //end jacobi loop
		#pragma omp single
			duration = omp_get_wtime() - start;

		#pragma omp critical(printingIters)
			printf("Final: Thread = %d, core = %d.\n", tid, sched_getcpu());

		#pragma omp single
			printf("Work item 0 pe = %d\n", workItems[0]->pe);

			

	} //end parallel section


/*===Printing and cleanup===*/
	printExperimentOutput(duration, workItems, nThreads*split, grids, nThreads*split, cols, rows, initGlobalNormR);


	#ifdef DUMP_AGGREGATE_OUTPUT
	for (int i = 0; i < nThreads; i++) {
		printf("Iters for thread %d = %d\n", i, threadIters[i]);
		//printf("Iters for core %d = %d\n", i, coreIters[i]);
	}
	#endif //DUMP_AGGREGATE_OUTPUT


	#ifdef SHORT_AGGREGATE_OUTPUT
	printf("Iterations per work item stats:\n");
	RealNumber* realIters = malloc(nThreads * sizeof(RealNumber));
	for (int i = 0; i < nThreads; i++) {
		realIters[i] = 0.0 + workItems[i]->updates;
	}
	printListStats(realIters, nThreads);
	free(realIters);
	
	printf("Iterations per core stats:\n");
	RealNumber* realCoreIters = malloc(nThreads * sizeof(RealNumber));
	for (int i = 0; i < nThreads; i++) {
		realCoreIters[i] = 0.0 + coreIters[i];
	}
	printListStats(realCoreIters, nThreads);
	free(realCoreIters);

	printf("Iterations per thread stats:\n");
	RealNumber* realThreadIters = malloc(nThreads * sizeof(RealNumber));
	for (int i = 0; i < nThreads; i++) {
		realThreadIters[i] = 0.0 + threadIters[i];
	}
	printListStats(realThreadIters, nThreads);
	free(realThreadIters);
	
	printf("Time per thread stats:\n");
	RealNumber* realTimes = malloc(nThreads * sizeof(RealNumber));
	for (int i = 0; i < nThreads; i++) {
		realTimes[i] = 0.0 + times[i];
	}
	printListStats(realTimes, nThreads);
	free(realTimes);
	#endif //SHORT_AGGREGATE_OUTPUT

	printGridsToFile(rowsG, colsG, grids, rows+2, cols+2, nThreads * split, npy * ysplit);
	return 0;
}
