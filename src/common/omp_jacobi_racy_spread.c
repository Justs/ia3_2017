/*   Copyright 2017 Justs Zarins

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>
#include <sched.h>
#include <unistd.h>
#include "precision.h"
//#include "ioutils.h"
#include "jacobi.h"
#include "jacobi_omp.h"
#include "thread_manager.h"
#include "utils.h"
#include "pgmio.h"
#include "settings.h"
#include <mpi.h>
#include "accumulator.h"
#include GSL

#define SEED 918237


/*
*  npx, npy number of PE's in x and y directions
*  myN local problem size (local domains are square)
*
*/

int omp_jacobi_racy_spread(int npx, int npy, int myN, RealNumber tolerance, int resCalcFreq, double managingFreqTime, int* workItemIters, RealNumber* residuals) {
	printf("Using racy spread Jacobi implementation.\n");
/*===Initialise variables===*/
	RealNumber scale = 100; // Larger means less diffuse input;
	int rowsG, colsG; //G stands for global
	int nThreads = npx * npy;
	
	int rows = myN; //local  rows
	int cols = myN; //local  cols
	rowsG = rows * npy; //global rows
	colsG = cols * npx; //global cols
	
	//arrays for intermediate reconstruction calculations, created with size +1 in each direction for halos
	RealNumber** grids = malloc(nThreads * sizeof(RealNumber*));
	RealNumber** newGrids = malloc(nThreads * sizeof(RealNumber*));
	RealNumber initVal = 1.0;
	RealNumber globalNormR = 0.0;
	RealNumber initGlobalNormR = 0.0;
	RealNumber globalAvg = -1;
	RealNumber goalVal = -1;

	//RealNumber* residuals = malloc(nThreads * sizeof(RealNumber));

	int* coreIters = calloc(nThreads, sizeof(int));
	int* threadIters = calloc(nThreads, sizeof(int));
	int* workIDs = malloc(nThreads * sizeof(int));
	double* times = malloc(nThreads * sizeof(double));
	double lastTime = 0;

	//locks to make sure two threads don't work on the same work item 
	omp_lock_t* workItemLocks = malloc(nThreads * sizeof(omp_lock_t));
	for (int i = 0; i < nThreads; i++)
		omp_init_lock(&workItemLocks[i]);

	const gsl_rng* r = gsl_rng_alloc(gsl_rng_mt19937);
	gsl_rng_set(r, SEED); //set seed;
	


	
	//this bit of code is to do INEFFICIENT master allocation (should create NUMA effects for memory accesses
	/*for (int t = 0; t < nThreads; t++) {
		grids[t] = malloc((rows + 2) * (cols + 2) * sizeof(RealNumber));
		newGrids[t] = malloc((rows + 2) * (cols + 2) * sizeof(RealNumber));
		for (int i = 0; i < cols+2; i++) {
			for (int j = 0; j < rows+2; j++) {
				grids[t][idx(rows+2, i, j)] = initVal;
				newGrids[t][idx(rows+2, i, j)] = initVal;
			}
		}
	}*/

/*===Start main parallel region===*/
	#pragma omp parallel num_threads(nThreads) default(none) shared(grids, newGrids, rows, cols, npx, npy, colsG, scale, nThreads, rowsG, globalNormR, initVal, tolerance, resCalcFreq, goalVal, residuals, workItemIters, workIDs, times, lastTime, coreIters, threadIters, workItemLocks, r, initGlobalNormR, managingFreqTime) 
	{
		int threadID = omp_get_thread_num();
		int workID, prevWorkID;
		int* coords, *neigh;
	

		workID = threadID;
		prevWorkID = workID;

		#pragma omp critical(printingIters)
			printf("Thread = %d, core = %d, initial workID = %d .\n", threadID, sched_getcpu(), workID);
		workIDs[threadID] = workID;

		//allocate main working arrays distributedly so they end up in memory banks near the CPUs that use them
		grids[workID] = malloc((rows + 2) * (cols + 2) * sizeof(RealNumber));
		newGrids[workID] = malloc((rows + 2) * (cols + 2) * sizeof(RealNumber));
		
		coords = malloc(2 * sizeof(int));
		neigh  = malloc(4 * sizeof(int));
		getCoords(workID, npy, coords);
		getNeighbours(workID, npx, npy, coords, neigh);

		RealNumber tempGlobalNormR;
		
		//execute initialisation in a set order to be repeatable
		#pragma omp for ordered schedule(static,1)
		for(int t = 0; t < nThreads; t++) {
			#pragma omp ordered
			initialiseGridOMP(rows, cols, colsG, grids[workID], coords, initVal, scale, r);
		}

		#pragma omp barrier
		getHalos(rows+2, cols+2, workID, neigh, grids); //need to get halos here in order to have correct residual calculations (if using random initialisation)
		#pragma omp barrier

		residuals[workID] = calcLocalResidual(rows+2, cols+2, grids[workID]);
		#pragma omp flush

		#pragma omp barrier

		#pragma omp single
		{
			printf("Global problem size: %d rows by %d columns.\nLocal problem size: %d rows by %d columns.\n ", rowsG, colsG, rows, cols);
			printf("Initial residual array:\n");
			for (int i = 0; i < nThreads; i++) 
				printf("%.14f ", residuals[i]);
			printf("\n");
			
			goalVal = tolerance; // * tolerance * globalNormB * globalNormB;
			for (int i = 0; i < nThreads; i++)
				globalNormR += residuals[i];//globalNormB * globalNormB;
			printf("pre-sqrt: %f\n", globalNormR);
			globalNormR = sqrt(globalNormR); // / globalNormB;
			initGlobalNormR = globalNormR;
			printf("initGlobalNormR=%f\n", initGlobalNormR);
			globalNormR /= initGlobalNormR;
			printf("Initial norm r = %f\n", globalNormR);
		}

/*===Iterate Jacobi===*/
		double start = omp_get_wtime();	
		while (globalNormR >= goalVal) {
			#ifdef MANAGE
			if (prevWorkID != workIDs[threadID]) {
			/*	#ifdef MANAGER_OUTPUT
				printf("Thread %d has changed from %d to %d.\n", threadID, prevWorkID, workIDs[threadID]);
				#endif //MANAGER_OUTPUT*/
	
				workID = workIDs[threadID];
				prevWorkID = workID;

				getCoords(workID, npy, coords);
				getNeighbours(workID, npx, npy, coords, neigh);
			}
			#endif //MANAGE

			omp_set_lock(&workItemLocks[workID]);

			getHalos(rows+2, cols+2, workID, neigh, grids);

			if (workID == 8 ) { //&& workItemIters[workID] % 2 == 0
				RealNumber skipPercentage = 0.048;
				updateGridPartially(rows, cols, newGrids[workID], grids[workID], skipPercentage, r);
			} else {
				updateGrid(rows, cols, newGrids[workID], grids[workID]);

			}
			copybackGrid(rows, cols, newGrids[workID], grids[workID]);
		
			//calculate how close the existing answer is to the real answer
			if (workItemIters[workID] % resCalcFreq == 0) {
				residuals[workID] = calcLocalResidual(rows+2, cols+2, grids[workID]);
				#pragma omp flush
			}

			#pragma omp single nowait 
			{
				if (workItemIters[workID] % resCalcFreq == 0) {
					RealNumber tempGlobalNormR = 0;
					for (int i = 0; i < nThreads; i++)
						tempGlobalNormR += residuals[i];
					globalNormR = sqrt(tempGlobalNormR) / initGlobalNormR;		
					#pragma omp flush(residuals)
					printf("Iteration: %i | Residual ~ %.14f\n", workItemIters[workID], globalNormR);
				}
			}

			
			workItemIters[workID]++;
			coreIters[sched_getcpu()]++;
			threadIters[threadID]++;

			omp_unset_lock(&workItemLocks[workID]);
			
			#ifdef MANAGE
			#pragma omp single nowait
			{
				//if (workItemIters[workID] % MANAGE_FREQ == 0) 
				if (omp_get_wtime() - lastTime > managingFreqTime) {
					//manageWorkItems(nThreads, workItemIters, workIDs);
					printf("MANAGER DISABLED\n");
					lastTime = omp_get_wtime();
				}
			}
			#endif //MANAGE

		} //end jacobi loop
		times[workID] = omp_get_wtime() - start;

		#pragma omp critical(printingIters)
			printf("Thread = %d, core = %d, final workID = %d .\n", threadID, sched_getcpu(), workID);
			

	} //end parallel section


/*===Printing and cleanup===*/
	globalAvg = 0.0;
	globalNormR = 0.0;
	for (int g = 0; g < nThreads; g++) {
		for (int i = 1; i < cols+1; i++) {
			for (int j = 1; j < rows+1; j++) {
				globalAvg += grids[g][idx(rows+2, i, j)];
			}
		}
		globalNormR += calcLocalResidual(rows+2, cols+2, grids[g]);
	}
	globalAvg /= rows*cols*nThreads;
	globalNormR = sqrt(globalNormR) / initGlobalNormR;
	printf("Avg cell value: %f Final residual: %.14f\n", globalAvg, globalNormR);


	#ifdef DUMP_AGGREGATE_OUTPUT
	for (int i = 0; i < nThreads; i++) {
		printf("Iters for work item %d = %d\n", i, workItemIters[i]);
		printf("Iters for thread %d = %d\n", i, threadIters[i]);
		printf("Iters for core %d = %d\n", i, coreIters[i]);
		printf("Time(s) for thread %d = %f\n", i, times[i]);
	}
	#endif //DUMP_AGGREGATE_OUTPUT


	#ifdef SHORT_AGGREGATE_OUTPUT
	printf("Iterations per work item stats:\n");
	RealNumber* realIters = malloc(nThreads * sizeof(RealNumber));
	for (int i = 0; i < nThreads; i++) {
		realIters[i] = 0.0 + workItemIters[i];
	}
	printListStats(realIters, nThreads);
	free(realIters);
	
	printf("Iterations per core stats:\n");
	RealNumber* realCoreIters = malloc(nThreads * sizeof(RealNumber));
	for (int i = 0; i < nThreads; i++) {
		realCoreIters[i] = 0.0 + coreIters[i];
	}
	printListStats(realCoreIters, nThreads);
	free(realCoreIters);

	printf("Iterations per thread stats:\n");
	RealNumber* realThreadIters = malloc(nThreads * sizeof(RealNumber));
	for (int i = 0; i < nThreads; i++) {
		realThreadIters[i] = 0.0 + threadIters[i];
	}
	printListStats(realThreadIters, nThreads);
	free(realThreadIters);
	
	printf("Time per thread stats:\n");
	RealNumber* realTimes = malloc(nThreads * sizeof(RealNumber));
	for (int i = 0; i < nThreads; i++) {
		realTimes[i] = 0.0 + times[i];
	}
	printListStats(realTimes, nThreads);
	free(realTimes);
	#endif //SHORT_AGGREGATE_OUTPUT

	printGridsToFile(rowsG, colsG, grids, rows+2, cols+2, nThreads, npy);
	//should free grids and newGrids...
	return 0;
}
