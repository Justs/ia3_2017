/*   Copyright 2017 Justs Zarins

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <jacobi.h>
#include <settings.h>
#include <math.h>

/* #include "mpi.h" */

void createfilename(char *filename, char *basename, int nx, int ny, int rank)
{
  if (rank < 0)
    {
      sprintf(filename, "%s%04dx%04d.dat", basename, nx, ny);
    }
  else
    {
      sprintf(filename, "%s%04dx%04d_%02d.dat", basename, nx, ny, rank);
    }
}


void iosize(char *filename, int *nx, int *ny)
{ 
  char *tmpname;

  tmpname = filename;

  while (*tmpname < '0' || *tmpname > '9')
  {
    tmpname++;
  }

  if (sscanf(tmpname, "%04dx%04d.dat", nx, ny) != 2)
  {
    printf("iosize: error parsing filename <%s>\n", filename);
    exit(1);
  }
}


void ioread(char *filename, void *ptr, int nfloat)
{
  int i;

  FILE *fp;
  
  float *data = (float *) ptr;

  printf("ioread: reading <%s> ...\n", filename);

  if ( (fp = fopen(filename, "r")) == NULL)
  {
    printf("ioread: failed to open input file <%s>\n", filename);
    exit(1);
  }

  if (fread(data, sizeof(float), nfloat, fp) != nfloat) 
  {
    printf("ioread: error reading input file <%s>\n", filename);
    exit(1);
  }

  fclose(fp);

  printf("... done\n", filename);
}

void iowrite(char *filename, void *ptr, int nfloat)
{
  int i;

  FILE *fp;
  
  float *data = (float *) ptr;

  printf("iowrite: writing <%s> ...\n", filename);

  if ( (fp = fopen(filename, "w")) == NULL)
  {
    printf("iowrite: failed to open output file <%s>\n", filename);
    exit(1);
  }

  if (fwrite(data, sizeof(float), nfloat, fp) != nfloat) 
  {
    printf("iowrite: error writing output file <%s>\n", filename);
    exit(1);
  }

  fclose(fp);
  printf("... done\n", filename);
}

#define INITDATAVAL 0.5

void initarray(void *ptr, int nx, int ny)
{
  int i, j;

  float *data = (float *) ptr;

  for (i=0; i < nx*ny; i++)
    {
      data[i] = INITDATAVAL;
    }
}

/*#define NDIM 2

void initpgrid(void *ptr, int nxproc, int nyproc)
{
  MPI_Comm gridcomm;
  MPI_Comm comm = MPI_COMM_WORLD;

  int dims[NDIM];
  int periods[NDIM] = {0, 0};

  int reorder = 0;

  int *pcoords = (int *) ptr;

  int i;

  dims[0] = nxproc;
  dims[1] = nyproc;

  MPI_Cart_create(comm, NDIM, dims, periods, reorder, &gridcomm);

  for (i=0; i < nxproc*nyproc; i++)
    {
      MPI_Cart_coords(gridcomm, i, NDIM, pcoords+NDIM*i);
    }

  MPI_Comm_free(&gridcomm);

}*/

void printExperimentOutput(double time, WorkItem** workItems, int nWorkItems, RealNumber** grids, int nGrids, int cols, int rows, RealNumber initGlobalNormR) {
	printf("Printing experiment output.\n");


	printf("Calculation time: %f\n", time);


        RealNumber globalAvg = 0.0;
        RealNumber globalNormR = 0.0;
        for (int g = 0; g < nGrids; g++) {
                for (int i = 1; i < cols+1; i++) {
                        for (int j = 1; j < rows+1; j++) {
                                globalAvg += grids[g][idx(rows+2, i, j)];
                        }
                }
                globalNormR += calcLocalResidual(rows+2, cols+2, grids[g]);
        }
	globalAvg /= rows*cols*nGrids;
	globalNormR = sqrt(globalNormR) / initGlobalNormR;
	
	printf("Avg cell value: %f Final residual: %.14f\n", globalAvg, globalNormR);


	for (int i = 0; i < nWorkItems; i++) {
		printf("Iters for work item %d = %d\n", i, workItems[i]->updates);
	}


	printf("Finished printing experiment output.\n");
}
