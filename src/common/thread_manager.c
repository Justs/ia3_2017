/*   Copyright 2017 Justs Zarins

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include "thread_manager.h"
#include "settings.h"
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>
#include <stdint.h>


int lastMin = -1;
int lastMax = -1;
void manageWorkItems(ManagerContext* ctx) {
	int nThreads = ctx->nThreads;
	int* iters = ctx->iters;
	int* workIDs = ctx->workIDs;

	//get fastest and slowest threads from iters
	//change their workID (let the threads themselves recalculate neighbours)
	int maxWID  = 0;
	int maxVal = -1;
	int minWID  = 0;
	int minVal = INT_MAX;

	//allow values to become out of date as the list is being searched
	for (int i = 0; i < nThreads; i++) {
		if (iters[i] > maxVal) {
			maxVal = iters[i];
			maxWID = i;
		}
		if (iters[i] < minVal) {
			minVal = iters[i];
			minWID = i;
		}
	}
	int maxTID = 0;
	int minTID = 0;

	for (int i = 0; i < nThreads; i++) {
		if (workIDs[i] == maxWID) {
			maxTID = i;
		} else if (workIDs[i] == minWID) {
			minTID = i;
		} 
	}

	//if (minWID != lastMin || maxWID != lastMax) {
		#ifdef MANAGER_OUTPUT
		//printf("swapping work items %d and %d on threads %d and %d (max and min)\n", workIDs[maxID], workIDs[minID], maxID, minID);
		printf("swapping work items %d and %d on threads %d and %d (max and min)\n", maxWID, minWID, maxTID, minTID);
		#endif //MANAGER_OUTPUT

		//int temp = workIDs[maxID];
		//workIDs[maxID] = workIDs[minID];
		//workIDs[minID] = temp;
		/*int temp = workIDs[maxTID];
		workIDs[maxTID] = workIDs[minTID];
		workIDs[minTID] = temp;*/
		workIDs[maxTID] = minWID;
		workIDs[minTID] = maxWID;

	//}
	#pragma omp flush
	
	//lastMin = minWID;
	//lastMax = maxWID;
}


int compareTI (const void * a, const void * b) {
   return ( (*(TwoInts*)a).iters - (*(TwoInts*)b).iters );
}

void manageWorkItemsGlobal(ManagerContext* ctx) {
	int nThreads = ctx->nThreads;
	int* iters = ctx->iters;
	int* workIDs = ctx->workIDs;
	
	
	TwoInts copy[nThreads];

	//allow values to become out of date as the list is being searched
	for (int i = 0; i < nThreads; i++) {
		copy[i].iters = iters[i];
		copy[i].id = i;
	}

	qsort(copy, nThreads, sizeof(TwoInts), compareTI);

	int temp, maxID, minID;
	for (int i = 0; i < nThreads/2; i++) {
		minID = copy[i].id;
		maxID = copy[nThreads-i-1].id;

		#ifdef MANAGER_OUTPUT
		printf("swapping work items %d and %d on threads %d and %d (max and min)\n", workIDs[maxID], workIDs[minID], maxID, minID);
		#endif //MANAGER_OUTPUT

		temp = workIDs[maxID];
		workIDs[maxID] = workIDs[minID];
		workIDs[minID] = temp;
	}
	#pragma omp flush

}

void manageSplitWorkItems(ManagerContext* ctx) {
	WorkItem** workItems = ctx->workItems;
	int nWIs = ctx->nWIs;
	int* dirty = ctx->dirty;


	int max = workItems[0]->updates;
	int min = max;
	int maxWI = 0;
	int minWI = 0;
	for (int i = 0; i < nWIs; i++) {
		if (workItems[i]->updates > max) {
			max = workItems[i]->updates;
			maxWI = i;
		}
		if (workItems[i]->updates < min) {
			min = workItems[i]->updates;
			minWI = i;
		}
	}
	int maxPE = workItems[maxWI]->pe;
	int minPE = workItems[minWI]->pe;

	if (minPE != maxPE) {
		#ifdef MANAGER_OUTPUT
			printf("Giving work item %d from pe %d to pe %d. maxWI=%d\n", minWI, minPE, maxPE, maxWI);
		#endif

		//int temp = workItems[maxWI]->pe;
		//workItems[maxWI]->pe = workItems[minWI]->pe;
		workItems[minWI]->pe = workItems[maxWI]->pe;
		dirty[minPE] = 1;
		dirty[maxPE] = 1;
	}

}


void manageSplitWorkItems2(ManagerContext* ctx) {
	WorkItem** workItems = ctx->workItems;
	int nWIs = ctx->nWIs;
	int* dirty = ctx->dirty;


	int max = workItems[0]->updates;
	int min = max;
	int maxWI = 0;
	int minWI = 0;
	for (int i = 0; i < nWIs; i++) {
		if (workItems[i]->updates > max) {
			max = workItems[i]->updates;
			maxWI = i;
		}
		if (workItems[i]->updates < min) {
			min = workItems[i]->updates;
			minWI = i;
		}
	}
	int maxPE = workItems[maxWI]->pe;
	int minPE = workItems[minWI]->pe;

	//////////////////
	int lenBotWIs = 0;
	int botWIs[nWIs];

	//find WIs on the same PE as the least update WI
	for (int i = 0; i < nWIs; i++) {
		if (i != minWI && workItems[i]->pe == minPE) {
			botWIs[lenBotWIs] = i;
			lenBotWIs++;
		}
	}

	//TODO: check for length of botWIs OR add managing lower/upper limits on WI count per PE
	//from those, pick the one with most updates
	int mostUpdatedBotWI = botWIs[0];
	max = workItems[mostUpdatedBotWI]->updates;

	for (int i = 0; i < lenBotWIs; i++) {
		if (workItems[botWIs[i]]->updates > max) {
			max = workItems[botWIs[i]]->updates;
			mostUpdatedBotWI = botWIs[i];
		}
	}

	//do transfer
	#ifdef MANAGER_OUTPUT
		printf("Giving work item %d from pe %d to pe %d. maxWI=%d\n", mostUpdatedBotWI, minPE, maxPE, maxWI);
	#endif

	workItems[mostUpdatedBotWI]->pe = maxPE;
	dirty[minPE] = 1;
	dirty[maxPE] = 1;


	//////////



}

int getNumberOfWIsOnPE(WorkItem** workItems, int nWIs, int pe) {
	int n = 0;
	for (int i = 0; i < nWIs; i++) {
		if (workItems[i]->pe == pe)
			n++;
	}

	return n;
}




/* Manage work items across memory banks.  
 * It relies on threads being mapped to cores with the same ID.
*/
void manageAcrossMemBanks(ManagerContext* ctx) {
	WorkItem** workItems = ctx->workItems;
	int nWIss = ctx->nWIs;
	int* dirty = ctx->dirty;
	int nPairs = ctx->nPairs;
	int lowerThresh = ctx->lowerThresh;
	int upperThresh = ctx->upperThresh;
	int splits = ctx->splits;
	
	
	//make lists of work items grouped by socket and get average number of updates per socket
	WorkItem* wis1[nWIss];  
	WorkItem* wis2[nWIss];
	int c1 = 0;
	int c2 = 0;
	double avg1 = 0;
	double avg2 = 0; 


	for (int i = 0; i < nWIss; i++) {
		if (workItems[i]->pe < ctx->numCores / 2) {
			wis1[c1] = workItems[i];
			c1++;
			avg1 += workItems[i]->updates;
		} else {
			wis2[c2] = workItems[i];
			c2++;
			avg2 += workItems[i]->updates;
		}
	}
	avg1 /= c1;
	avg2 /= c2;



	//give random WI from list with more average updates to random core on the other socket 
	//may not succed in finding a good pair the first time.
	int pe1 = -1;
	int pe2 = -1;
	while (pe1 == -1 && pe2 == -1) {
		int id1 = rand() % c1;
		int id2 = rand() % c2;

		pe1 = wis1[id1]->pe;
		pe2 = wis2[id2]->pe;
		int n1 = getNumberOfWIsOnPE(workItems, nWIss, pe1);
		int n2 = getNumberOfWIsOnPE(workItems, nWIss, pe2); 

		if (avg1 > avg2) {
			if (n2 >= lowerThresh && n1 <= (upperThresh - 2)) {
				wis2[id2]->pe = wis1[id1]->pe;
			} else {
				pe1 = -1;
				pe2 = -1;
			}
		} else {
			if (n1 >= lowerThresh && n2 <= (upperThresh - 2)) {
				wis1[id1]->pe = wis2[id2]->pe;
			} else {
				pe1 = -1;
				pe2 = -1;
			}
		}
	}

	dirty[pe1] |= 1;
	dirty[pe2] |= 1;

}

void manageSplitWorkItems3(ManagerContext* ctx) {
	WorkItem** workItems = ctx->workItems;
	int nWIss = ctx->nWIs;
	int* dirty = ctx->dirty;
	int nPairs = ctx->nPairs;
	int lowerThresh = ctx->lowerThresh;
	int upperThresh = ctx->upperThresh;
	int splits = ctx->splits;
	int crossSocketFreq = ctx->crossSocketFreq;


	TwoInts copies[nWIss];
	for (int i = 0; i < nWIss; i++) {
		copies[i].iters = workItems[i]->updates;
		copies[i].id = i;
	}

	int nThreads = omp_get_num_threads();
	int dirties[nThreads];
	for (int i = 0; i < nThreads; i++)
		dirties[i] = 0;


	for (int z = 0; z < splits; z++) {
		int nWIs = nWIss;
		TwoInts copy[nWIs];

		int nCores = ctx->numCores;
		int minPE = nCores/splits * z;
		int maxPE = nCores/splits * (z + 1);

		int c = 0;
		//allow values to become out of date as the list is being searched
		for (int i = 0; i < nWIss; i++) {
			if (minPE <= workItems[i]->pe && workItems[i]->pe < maxPE) { 
				copy[c].iters = copies[i].iters;
				copy[c].id = copies[i].id;
				c++;
			}
		}
		nWIs = c; //adjust max length of list

		qsort(copy, nWIs, sizeof(TwoInts), compareTI);

		#ifdef MANAGER_OUTPUT
			printf("Thread %d managing. Min iters: %d. Max iters: %d.\n", omp_get_thread_num(), copy[0].iters, copy[nWIs - 1].iters);
		#endif

		for (int i = 0; i < nPairs; i++) {
			int maxWI = copy[nWIs - i - 1].id;
			int minWI = copy[i].id;

			int maxPE = workItems[maxWI]->pe;
			int minPE = workItems[minWI]->pe;

			int lenBotWIs = 0;
			int botWIs[nWIs];

			int lenTopWIs = 0;
			int topWIs[nWIs];

			//find WIs on the same PE as the least update WI
			for (int ii = 0; ii < nWIs; ii++) {
				int e = copy[ii].id;
				if (e != minWI && workItems[e]->pe == minPE) {
					botWIs[lenBotWIs] = e;
					lenBotWIs++;
				}

				if (e != maxWI && workItems[e]->pe == maxPE) {
					topWIs[lenTopWIs] = e;
					lenTopWIs++;
				}
			}

			if (lenBotWIs >= lowerThresh && lenTopWIs <= (upperThresh - 2)) {

				//from those, pick the one with most updates
				int mostUpdatedBotWI = botWIs[0];
				int max = workItems[mostUpdatedBotWI]->updates;

				for (int j = 0; j < lenBotWIs; j++) {
					if (workItems[botWIs[j]]->updates > max) {
						max = workItems[botWIs[j]]->updates;
						mostUpdatedBotWI = botWIs[j];
					}
				}

					//do transfer
					#ifdef MANAGER_OUTPUT
						printf("%d Giving work item %d from pe %d to pe %d. maxWI=%d\n", i, mostUpdatedBotWI, minPE, maxPE, maxWI);
					#endif

					workItems[mostUpdatedBotWI]->pe = maxPE;
					
					dirties[minPE] = 1;
					dirties[maxPE] = 1;
			} else {
				#ifdef MANAGER_OUTPUT
					printf("%d Did not do swap. %d, %d, %d, %d \n", i, lenBotWIs, lenTopWIs, minPE, maxPE);
				 #endif
			}


		}
	}


	if (crossSocketFreq != -1) {
		ctx->crossSocketCounter++;
		if (ctx->crossSocketCounter % crossSocketFreq == 0)
			manageAcrossMemBanks(ctx);
	}


	for (int i = 0; i < nThreads; i++)
		dirty[i] |= dirties[i];
}
