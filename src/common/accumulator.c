/*   Copyright 2017 Justs Zarins

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <unistd.h>
#include <sched.h>
#include <stdio.h>
#include <mpi.h>


int accumulate(int size, int freq) {
	double acc = 0.0;
	int i;
	int inMessage, flag;
	MPI_Request request;
	MPI_Status status;

	//post non-blocking receive
	MPI_Irecv(&inMessage, 1, MPI_INT, 0, 0, MPI_COMM_WORLD, &request);
	
	while(1) {
		for (i = 0; i < size; i++)
			acc = acc*1.000001 + 0.0001;
		usleep(freq);
		//check if I should finish
		MPI_Test(&request, &flag, &status);
		if (flag) break;
	}

	return 0;
}

int accumulate_nompi(int size, int freq, volatile int* flag) {
	double acc = 0.0;
	int i;
	
	while(1) {
		for (i = 0; i < size; i++)
		acc = acc*1.000001 + 0.0001;
		usleep(freq);
		//#pragma omp flush(flag)
		if (! *flag) break;
	}
	
	return 0;
}
