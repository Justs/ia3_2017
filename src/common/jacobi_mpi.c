/*   Copyright 2017 Justs Zarins

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include "precision.h"
#include "jacobi_mpi.h"
#include "jacobi.h"
#include "ioutils.h"
#include "pgmio.h"



void swapHalos(int rows, int cols, RealNumber* old, int rightRank, int leftRank, int upRank, int downRank, MPI_Comm *com, MPI_Datatype *horizontalHalo, MPI_Request *request, MPI_Status *status) {
		/*//left -> right
		MPI_Issend(&old[idx(cols, 1, cols-2)], 1, *horizontalHalo, rightRank, 1, *com, request);
		MPI_Recv(&old[idx(cols, 1, 0)], 1, *horizontalHalo, leftRank, 1, *com, status);
		MPI_Wait(request, MPI_STATUS_IGNORE);		

		//right -> left
		MPI_Issend(&old[idx(cols, 1, 1)], 1, *horizontalHalo, leftRank, 1, *com, request);	
		MPI_Recv(&old[idx(cols, 1, cols-1)], 1, *horizontalHalo, rightRank, 1, *com, status);
		MPI_Wait(request, MPI_STATUS_IGNORE);
	
		//down -> up
		MPI_Issend(&old[idx(cols, 1, 1)], cols-2, MPI_REALNUMBER, upRank, 1, *com, request);
		MPI_Recv(&old[idx(cols, rows-1, 1)], cols-2, MPI_REALNUMBER, downRank, 1, *com, status);
		MPI_Wait(request, MPI_STATUS_IGNORE);

		//up -> down
		MPI_Issend(&old[idx(cols, rows-2, 1)], cols-2, MPI_REALNUMBER, downRank, 1, *com, request);		
		MPI_Recv(&old[idx(cols, 0, 1)], cols-2, MPI_REALNUMBER, upRank, 1, *com, status);
		MPI_Wait(request, MPI_STATUS_IGNORE);
		*/

 // sizeX->rows, sizeY->cols
		//left -> right
		MPI_Issend(&old[idx(rows, cols-2, 1)], rows-2, MPI_REALNUMBER, rightRank, 1, *com, request);
		MPI_Recv(&old[idx(rows, 0, 1)], rows-2, MPI_REALNUMBER, leftRank, 1, *com, status);
		MPI_Wait(request, MPI_STATUS_IGNORE);		

		//right -> left
		MPI_Issend(&old[idx(rows, 1, 1)], rows-2, MPI_REALNUMBER, leftRank, 1, *com, request);		
		MPI_Recv(&old[idx(rows, cols-1, 1)], rows-2, MPI_REALNUMBER, rightRank, 1, *com, status);
		MPI_Wait(request, MPI_STATUS_IGNORE);
	
		//down -> up
		MPI_Issend(&old[idx(rows, 1, rows-2)], 1, *horizontalHalo, upRank, 1, *com, request);		
		MPI_Recv(&old[idx(rows, 1, 0)], 1, *horizontalHalo, downRank, 1, *com, status);
		MPI_Wait(request, MPI_STATUS_IGNORE);

		//up -> down
		MPI_Issend(&old[idx(rows, 1, 1)], 1, *horizontalHalo, downRank, 1, *com, request);		
		MPI_Recv(&old[idx(rows, 1, rows-1)], 1, *horizontalHalo, upRank, 1, *com, status);
		MPI_Wait(request, MPI_STATUS_IGNORE);

}



RealNumber calcAvgPixelVal(int rows, int cols, RealNumber* grid, MPI_Comm *com, int size) {
	RealNumber localAvg = 0;
	RealNumber globalAvg = 0;
	for (int i = 1; i < cols-1; i++) {
		for (int j = 1; j < rows-1; j++) {
				localAvg += grid[idx(rows, i, j)];
		}
	}
	localAvg /= (rows-2)*(cols-2);
	MPI_Allreduce(&localAvg, &globalAvg, 1, MPI_REALNUMBER, MPI_SUM, *com);
	return globalAvg / size;
}


/*
RealNumber calcMaxDelta(int sizeX, int sizeY, RealNumber new[sizeX][sizeY], RealNumber old[sizeX][sizeY], MPI_Comm *com) {
	RealNumber localMaxDelta = 0;
	RealNumber globalMaxDelta = 0;

	RealNumber delta = 0;
	for (int i = 1; i < sizeX-1; i++) {
		for (int j = 1; j < sizeY-1; j++) {
				delta = fabsf(new[i][j] - old[i][j]);
				if (delta > localMaxDelta)
					localMaxDelta = delta;
		}
	}

	MPI_Allreduce(&localMaxDelta, &globalMaxDelta, 1, MPI_REALNUMBER, MPI_MAX, *com);

	return globalMaxDelta;			
}
*/

RealNumber calcResidual(int rows, int cols, RealNumber* grid, RealNumber globalNormB, MPI_Comm *com) {
	RealNumber normR = 0.0;
	RealNumber globalNormR = 0.0;
	RealNumber temp = -1;
	for (int i = 1; i < cols-1; i++) {
		for (int j = 1; j < rows-1; j++) {
			temp = 4 * grid[idx(rows, i, j)] - 
				   grid[idx(rows, i+1, j)] - 
				   grid[idx(rows, i-1, j)] - 
				   grid[idx(rows, i, j+1)] - 
				   grid[idx(rows, i, j-1)];
			normR += temp * temp;
		}
	}

	MPI_Allreduce(&normR, &globalNormR, 1, MPI_REALNUMBER, MPI_SUM, *com);

	return sqrt(globalNormR) / globalNormB;
}


#define BASENAME "parallelOut"
void printGridToFile(int rows, int cols, RealNumber* grid, int rowsG, int colsG, int* dims, MPI_Comm* com) {
	MPI_Status status;
	int rank;
	MPI_Comm_rank(*com, &rank);

	if (rank == 0)
		printf("Printing grid to file...\n");

	RealNumber* buf = malloc(rows * cols * sizeof(RealNumber));
	//copy reconstructed sub-image into buffer
	for (int i = 1; i < cols+1; i++) {
		for (int j = 1; j < rows+1; j++) {
			if (rank == 4)
				buf[idx(rows, i-1, j-1)] = rank * 10;//grid[idx(rows+2, i, j)];
			else
				buf[idx(rows, i-1, j-1)] = 0;
		}
	}

    
	//define a subarray datatype for sub-images
	MPI_Datatype subImg;
   	int sizes[2], subsizes[2], starts[2];
	//sizes[0] = M+2; sizes[1] = N+2;
	sizes[0] = colsG; sizes[1] = rowsG;
	subsizes[0] = cols; subsizes[1] = rows;
	int order = MPI_ORDER_C;
 
	int coords2[2]; 
	MPI_Cart_coords(*com, rank, 2, coords2); 
	//int displacementX = M / dims[0] * coords2[0] + 1;
	//int displacementY = N / dims[1] * coords2[1] + 1;
	int displacementX = colsG / dims[0] * coords2[0];
	int displacementY = rowsG / dims[1] * coords2[1];

	starts[0] = displacementX; starts[1] = displacementY;

	printf("Rank: %d; disX=%d disY=%d.\n", rank, displacementX, displacementY);
	    
	MPI_Type_create_subarray(2, sizes, subsizes, starts, order, MPI_REALNUMBER, &subImg);
	MPI_Type_commit(&subImg);    
    
    
	//write data
	char outputName[1024];
	//createfilename(outputName, BASENAME, M+2, N+2, -1);
	createfilename(outputName, BASENAME, rowsG, colsG, -1);
	int amode = MPI_MODE_CREATE | MPI_MODE_WRONLY;
	MPI_File fh;
	MPI_File_open(*com, outputName, amode, MPI_INFO_NULL, &fh);
	
	MPI_File_set_view(fh, 0, MPI_REALNUMBER, subImg, "native", MPI_INFO_NULL);
    
	MPI_File_write_all(fh, buf, rows*cols, MPI_REALNUMBER, &status);
	

/*
	//write global halo

	//data
	RealNumber mArr[M+2];
	RealNumber nArr[N+2];
	for (int i = 0; i < M+2; i++) {
		mArr[i] = 0;
	}
	for (int i = 0; i < N+2; i++) {
		nArr[i] = 0;
	}
	    
	//subarrays
	//UP
    	MPI_Datatype upS;
        sizes[0] = M+2; sizes[1] = N+2;
        subsizes[0] = M+2; subsizes[1] = 1;

        starts[0] = 0;
        starts[1] = N+1;
    	    
        MPI_Type_create_subarray(2, sizes, subsizes, starts, order, MPI_REALNUMBER, &upS);
        MPI_Type_commit(&upS);    
        
        MPI_File_set_view(fh, 0, MPI_REALNUMBER, upS, "native", MPI_INFO_NULL);
        if (rank == 0)
        	MPI_File_write(fh, mArr, M+2, MPI_REALNUMBER, &status);
        
        //DOWN
        MPI_Datatype downS;
        subsizes[0] = M+2; subsizes[1] = 1;

        starts[0] = 0;
        starts[1] = 0;
    	    
        MPI_Type_create_subarray(2, sizes, subsizes, starts, order, MPI_REALNUMBER, &downS);
        MPI_Type_commit(&downS);    
        
        MPI_File_set_view(fh, 0, MPI_REALNUMBER, downS, "native", MPI_INFO_NULL);
        if (rank == 0)
        	MPI_File_write(fh, mArr, M+2, MPI_REALNUMBER, &status);
        
        
        //LEFT
        MPI_Datatype leftS;
        subsizes[0] = 1; subsizes[1] = N+2;

        starts[0] = 0;
        starts[1] = 0;
    	    
        MPI_Type_create_subarray(2, sizes, subsizes, starts, order, MPI_REALNUMBER, &leftS);
        MPI_Type_commit(&leftS);    
        
        MPI_File_set_view(fh, 0, MPI_REALNUMBER, leftS, "native", MPI_INFO_NULL);
        if (rank == 0)
        	MPI_File_write(fh, nArr, N+2, MPI_REALNUMBER, &status);
        
        
        //RIGHT
        MPI_Datatype rightS;
        subsizes[0] = 1; subsizes[1] = N+2;

        starts[0] = M+1;
        starts[1] = 0;
    	    
        MPI_Type_create_subarray(2, sizes, subsizes, starts, order, MPI_REALNUMBER, &rightS);
        MPI_Type_commit(&rightS);    
        
        MPI_File_set_view(fh, 0, MPI_REALNUMBER, rightS, "native", MPI_INFO_NULL);
        if (rank == 0)
        	MPI_File_write(fh, nArr, N+2, MPI_REALNUMBER, &status);

*/

	MPI_File_close(&fh);
	free(buf);
	
	if (rank == 0)
		printf("Finished printing grid to file %s\n", outputName);

}
