/*   Copyright 2017 Justs Zarins

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include "precision.h"
#include "utils.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include "thread_manager.h"
#include "settings.h"



int compareReals (const void * a, const void * b) {
   return ( *(RealNumber*)a - *(RealNumber*)b );
}

void printListStats(RealNumber* list, int n) {
	RealNumber* copy = malloc(n * sizeof(RealNumber));
	for (int i = 0; i < n; i++)
		copy[i] = list[i];
	qsort(copy, n, sizeof(RealNumber), compareReals);
	
	RealNumber min = copy[0];
	RealNumber max = copy[n-1];
	
	RealNumber median = 0;
	if (n % 2 != 0) median = copy[(n-1)/2];
	else 		median = (copy[(n-1)/2] + copy[n/2]) / 2;

	RealNumber mean = 0;
	for (int i = 0; i < n; i++)
		mean += copy[i];

	mean /= n;

	RealNumber stddev = 0;
	for (int i = 0; i < n; i++) 
		stddev += (copy[i] - mean)*(copy[i] - mean);
	stddev = sqrt(stddev / n);

	printf("min=%.2f median=%.2f mean=%.2f max=%.2f stddev=%.2f\n", min, median, mean, max, stddev);

	free(copy);
}


Manager* initManager(Config* c) {
	Manager* m = malloc(sizeof(Manager));
	ManagerContext* ctx = malloc(sizeof(ManagerContext));
    
    ctx->crossSocketCounter = 0;
    ctx->nPairs = c->nPairs;
    ctx->lowerThresh = c->lowerThresh;
    ctx->upperThresh = c->upperThresh;
    ctx->maxStaleness = c->maxStaleness;
    ctx->splits = c->splits;
    ctx->crossSocketFreq = c->crossSocketFreq;
    ctx->numCores = c->numCores;
	
    m->ctx = ctx;
    m->managingFreqTime = c->managingFreqTime;
	
    switch(c->managerType) {
		case 0:
			m->manage = &manageWorkItems;
			break;
		case 1:
			m->manage = &manageWorkItemsGlobal;
			break;
		case 2:
			m->manage = &manageSplitWorkItems;
			break;
		case 3:
			m->manage = &manageSplitWorkItems2;
			break;
		case 4:
			m->manage = &manageSplitWorkItems3;
			break;
		default:
			printf("Could not parse manager type: %d.\nExiting.", c->managerType);
			exit(1);
	}

	return m;
}


void printDefinedVars() {
	#ifdef MANAGE
		printf("Pacemaker enabled.\n");
	#else
		printf("Pacemaker disabled.\n");
	#endif


	#ifdef MANAGER_OUTPUT
		printf("Pacemaker output enabled.\n");
	#else
		printf("Pacemaker output disabled.\n");
	#endif
	
	#ifdef SAMPLER
		printf("Sampler enabled.\n");
	#else
		printf("Sampler disabled.\n");
	#endif

}

Config* parseConfig(char* filename) {
    int maxLen = 1024;
    Config* c = malloc(sizeof(Config));
    char* buffer = malloc(maxLen * sizeof(char));
	
    FILE* f = fopen(filename, "r");
    if (f == NULL) {
        printf("Error opening file %s. Exiting.\n", filename);
        exit(-1);
    }

    int integer;
    double floating;
    char* name = malloc(sizeof maxLen * sizeof(char));
    int codeI = -1, codeF = -1;
    while(fgets(buffer, maxLen, f) != NULL) {
        if (buffer[0] != '#') {
            codeI = sscanf(buffer, "%s %d", name, &integer);
            codeF = sscanf(buffer, "%s %lf", name, &floating);
        } else {
            codeI = -1;
            codeF = -1;
        }

        if (codeI > 0 || codeF > 0) {
            if (strcmp(name, "jacobiType") == 0) {
                c->jacobiType = integer;
            } else if (strcmp(name, "npx") == 0) {
                c->npx = integer;
            } else if (strcmp(name, "npy") == 0) {
                c->npy = integer;
            } else if (strcmp(name, "myN") == 0) {
                c->myN = integer;
            } else if (strcmp(name, "tolerance") == 0) {
                c->tolerance = floating;
            } else if (strcmp(name, "maxIters") == 0) {
                c->maxIters = integer;
            } else if (strcmp(name, "resCalcFreq") == 0) {
                c->resCalcFreq = integer;
            } else if (strcmp(name, "enableNoise") == 0) {
                c->enableNoise = integer;
            } else if (strcmp(name, "noiseSize") == 0) {
                c->noiseSize = integer;
            } else if (strcmp(name, "noiseFreq") == 0) {
                c->noiseFreq = integer;
            } else if (strcmp(name, "xsplit") == 0) {
                c->xsplit = integer;
            } else if (strcmp(name, "ysplit") == 0) {
                c->ysplit = integer;
            } else if (strcmp(name, "managerType") == 0) {
                c->managerType = integer;
            } else if (strcmp(name, "managingFreqTime") == 0) {
                c->managingFreqTime = floating;
            } else if (strcmp(name, "nPairs") == 0) {
                c->nPairs = integer;
            } else if (strcmp(name, "lowerThresh") == 0) {
                c->lowerThresh = integer;
            } else if (strcmp(name, "upperThresh") == 0) {
                c->upperThresh = integer;
            } else if (strcmp(name, "maxStaleness") == 0) {
                c->maxStaleness = integer;
            } else if (strcmp(name, "splits") == 0) {
                c->splits = integer;
            } else if (strcmp(name, "crossSocketFreq") == 0) {
                c->crossSocketFreq = integer;
            } else if (strcmp(name, "numCores") == 0) {
                c->numCores = integer;
            } else {
                printf("Could not parse config variable: %s\n", name);
            }
        }
        
    }


    fclose(f);
    free(buffer);
    free(name);
    return c;
}


void printConfig(Config* c) {
	printf("Confinguration.\n \
    jacobi type: %d\n \
    npx: %d\n \
    npy: %d\n \
    myN: %d\n \
    tolerance: %f\n \
    maxIters: %d\n \
    resCalcFreq: %d\n \
    enableNoise: %d\n \
    noiseSize: %d\n \
    noiseFreq: %d\n \
    xsplit: %d\n \
    ysplit: %d\n \
	managerType: %d\n \
	managinfFreqTime: %f\n \
	nPairs: %d\n \
	lowerThresh: %d\n \
	upperThresh: %d\n \
	maxStaleness: %d\n \
	splits: %d\n \
	crossSocketFreq: %d\n \
	numCores: %d\n", 
        c->jacobiType,
        c->npx,
        c->npy,
        c->myN,
        c->tolerance,
        c->maxIters,
        c->resCalcFreq,
        c->enableNoise,
        c->noiseSize,
        c->noiseFreq,
        c->xsplit,
        c->ysplit,		
        c->managerType,
		c->managingFreqTime,
		c->nPairs,
		c->lowerThresh,
		c->upperThresh,
		c->maxStaleness,
		c->splits,
		c->crossSocketFreq,
		c->numCores);
    
}
