/*   Copyright 2017 Justs Zarins

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>
#include <sched.h>
#include "precision.h"
#include "ioutils.h"
#include "jacobi.h"
#include "jacobi_omp.h"
#include "pgmio.h"
#include "settings.h"
#include GSL

#define SEED 918237
		


int omp_jacobi_semi_sync(int npx, int npy, int myN, RealNumber tolerance, int maxIters, int resCalcFreq, int maxStaleness, WorkItem** workItems) {
	printf("Using semi synchronous Jacobi implementation with %d max staleness bound.\n", maxStaleness);
/*===Initialise variables===*/
	RealNumber scale = 100; // Larger means less diffuse input;
	
	int rowsG, colsG;	//G stands for global
	int nThreads = npx * npy;
	
	int rows = myN; //local  rows
	int cols = myN; //local  cols
	rowsG = rows * npy; //global rows
	colsG = cols * npx; //global cols

	const gsl_rng* r = gsl_rng_alloc(gsl_rng_mt19937);
        gsl_rng_set(r, SEED); //set seed;
	
	//arrays for intermediate reconstruction calculations, created with size +1 in each direction for halos
	RealNumber** grids = malloc(nThreads * sizeof(RealNumber*));
	RealNumber** newGrids = malloc(nThreads * sizeof(RealNumber*));
	RealNumber initVal = 1.0;
	RealNumber globalNormR = 0.0;
	RealNumber initGlobalNormR = 0.0;
	RealNumber globalAvg = -1;

	RealNumber* residuals = calloc(nThreads, sizeof(RealNumber));

	volatile int* versions = calloc(nThreads * 4, sizeof(unsigned int));

	RealNumber** upBufs    = malloc(nThreads * sizeof(RealNumber*));
	RealNumber** downBufs  = malloc(nThreads * sizeof(RealNumber*));
	RealNumber** leftBufs  = malloc(nThreads * sizeof(RealNumber*));
	RealNumber** rightBufs = malloc(nThreads * sizeof(RealNumber*));
	
	omp_lock_t* upLocks    = malloc(nThreads * sizeof(omp_lock_t));
	omp_lock_t* downLocks  = malloc(nThreads * sizeof(omp_lock_t));
	omp_lock_t* leftLocks  = malloc(nThreads * sizeof(omp_lock_t));
	omp_lock_t* rightLocks = malloc(nThreads * sizeof(omp_lock_t));
	for (int i = 0; i < nThreads; i++) {
		omp_init_lock(&upLocks[i]);
		omp_init_lock(&downLocks[i]);
		omp_init_lock(&leftLocks[i]);
		omp_init_lock(&rightLocks[i]);
	}

	double duration;
	volatile int shouldContinue = 1;

/*===Start main parallel region===*/
	#pragma omp parallel num_threads(nThreads) default(none) shared(grids, newGrids, rows, cols, npx, npy, colsG, scale, nThreads, rowsG, globalNormR, initVal, tolerance, maxIters, resCalcFreq, duration, upBufs, downBufs, leftBufs, rightBufs, maxStaleness, upLocks, downLocks, leftLocks, rightLocks, residuals, initGlobalNormR, shouldContinue, workItems, versions) 
	{
		#ifdef INIT_BY_THREAD_ID
		int threadID = omp_get_thread_num();
		#else
		int threadID = sched_getcpu();
		#endif
		
		int wid = threadID;
		grids[wid] = malloc((rows + 2) * (cols + 2) * sizeof(RealNumber));
		newGrids[wid] = malloc((rows + 2) * (cols + 2) * sizeof(RealNumber));

		//1 more spot added to hold halo's version
		upBufs[wid]    = calloc((cols+1), sizeof(RealNumber));
		downBufs[wid]  = calloc((cols+1), sizeof(RealNumber));
		leftBufs[wid]  = calloc((rows+1), sizeof(RealNumber));
		rightBufs[wid] = calloc((rows+1), sizeof(RealNumber));
		
		WorkItem* wi = malloc(sizeof(WorkItem));
		wi->id = wid;
		wi->pe = threadID;
		wi->updates = 0;
		wi->residual = -1;
		omp_init_lock(&(wi->lock));
		
		getCoords(threadID, npy, wi->coords);
		getNeighbours(threadID, npx, npy, wi->coords, wi->neigh);

		workItems[wid] = wi;
	
		initialiseGridOMP(rows, cols, colsG, grids[wid], workItems[wid]->coords, initVal, scale, NULL);
		
		workItems[wid]->residual = setupResidualsSync(rows, cols, grids[threadID], &globalNormR, &initGlobalNormR);
		printf("Thread %d residual %f\n", threadID, workItems[threadID]->residual);
		#pragma omp barrier //to make sure all residuals get recorded


		


		/*residuals[threadID] = calcLocalResidual(rows+2, cols+2, grids[threadID]);
		#pragma omp atomic
			globalNormR += residuals[threadID];
		#pragma omp barrier

		#pragma omp single
		{
			initGlobalNormR = sqrt(globalNormR);
			printf("sqrt((b-Ax)**2) = %f\n", initGlobalNormR);
			globalNormR = sqrt(globalNormR) / initGlobalNormR;
			printf("Initial norm r = %f\n", globalNormR);
		}*/
		
		#pragma omp single nowait
		{
			printf("Running on %d threads.\nGlobal problem size: %d rows by %d columns.\nLocal problem size: %d rows by %d columns.\n ", nThreads, rowsG, colsG, rows, cols);
			printf("sqrt((b-Ax)**2) = %f\n", initGlobalNormR);
			printf("Initial norm r = %f\n", globalNormR);
		}
	

		int iter;
/*===Iterate Jacobi===*/

		#pragma omp single	
			duration = omp_get_wtime();

		while (shouldContinue == 1) {
			int* neigh = workItems[wid]->neigh;
			//dispatch halos
			if (neigh[0] > -1) {
				//omp_set_lock(&leftLocks[threadID]);
					
				for (int j = 1; j < rows+1; j++)
					leftBufs[threadID][j - 1] = grids[threadID][idx(rows+2, 1, j)];
				versions[threadID*4 + 0]++;
				///leftBufs[threadID][rows]++;

				//omp_unset_lock(&leftLocks[threadID]);
			}
			
			if (neigh[1] > -1) {
				//omp_set_lock(&rightLocks[threadID]);

				for (int j = 1; j < rows+1; j++)
					rightBufs[threadID][j - 1] = grids[threadID][idx(rows+2, cols, j)];
				versions[threadID*4 + 1]++;
				///rightBufs[threadID][rows]++;

				//omp_unset_lock(&rightLocks[threadID]);
			}
			
			
			if (neigh[2] > -1) {
				//omp_set_lock(&upLocks[threadID]);

				for (int i = 1; i < cols+1; i++)
					upBufs[threadID][i - 1] = grids[threadID][idx(rows+2, i, rows)];
				versions[threadID*4 + 2]++;
				///upBufs[threadID][cols]++;

				//omp_unset_lock(&upLocks[threadID]);
			}
			
			if (neigh[3] > -1) {
				//omp_set_lock(&downLocks[threadID]);

				for (int i = 1; i < cols+1; i++)
					downBufs[threadID][i - 1] = grids[threadID][idx(rows+2, i, 1)];
				versions[threadID*4 + 3]++;
				///downBufs[threadID][cols]++;

				//omp_unset_lock(&downLocks[threadID]);
			}

			//get halos

			//get from LEFT
			if (neigh[0] > -1) {
				while (shouldContinue) {
					//omp_set_lock(&rightLocks[neigh[0]]);
					///if (workItems[wid]->updates - rightBufs[neigh[0]][rows] > maxStaleness) {
					if (workItems[wid]->updates - versions[neigh[0]*4 + 1] > maxStaleness) {
						//release lock to give neighbour a chance to dispatch a new version of its halos			
						//omp_unset_lock(&rightLocks[neigh[0]]);
						//usleep(100);
						//printf("wid %d stuck: %d\n", wid, workItems[wid]->updates - versions[neigh[0]*4 +1] );
					} else {
						//printf("wid %d not stuck: %d\n", wid, workItems[wid]->updates - versions[neigh[0]*4 + 1] );
						break;
					} 
				}

				for (int j = 1; j < rows+1; j++)
					grids[threadID][idx(rows+2, 0, j)] = rightBufs[neigh[0]][j-1];

				//omp_unset_lock(&rightLocks[neigh[0]]);
			}
			
			//get from RIGHT
			if (neigh[1] > -1) {
				while (shouldContinue) {
					//omp_set_lock(&leftLocks[neigh[1]]);
					///if (workItems[wid]->updates - leftBufs[neigh[1]][rows] > maxStaleness) {
					if (workItems[wid]->updates - versions[neigh[1]*4 + 0] > maxStaleness) {
						//omp_unset_lock(&leftLocks[neigh[1]]);
						//usleep(100);
					} else {
						break;
					} 
				}

				for (int j = 1; j < rows+1; j++)
					grids[threadID][idx(rows+2, cols+1, j)] = leftBufs[neigh[1]][j-1];
				
				//omp_unset_lock(&leftLocks[neigh[1]]);
			}
			
			
			//get from UP
			if (neigh[2] > -1) {
				while (shouldContinue) {
					//omp_set_lock(&downLocks[neigh[2]]);
					///if (workItems[wid]->updates - downBufs[neigh[2]][cols] > maxStaleness) {
					if (workItems[wid]->updates - versions[neigh[2]*4 + 3] > maxStaleness) {
						//omp_unset_lock(&downLocks[neigh[2]]);
						//usleep(100);
					} else {
						break;
					} 
				}

				for (int i = 1; i < cols+1; i++)
					grids[threadID][idx(rows+2, i, rows+1)] = downBufs[neigh[2]][i-1];
						
				//omp_unset_lock(&downLocks[neigh[2]]);
			}
			
			//get from DOWN
			if (neigh[3] > -1) {
				while (shouldContinue) {
					//omp_set_lock(&upLocks[neigh[3]]);
					///if (workItems[wid]->updates - upBufs[neigh[3]][cols] > maxStaleness) {
					if (workItems[wid]->updates - versions[neigh[3]*4 + 2] > maxStaleness) {
						//omp_unset_lock(&upLocks[neigh[3]]);
						//usleep(100);
					} else {
						break;
					} 
				}
				for (int i = 1; i < cols+1; i++)
					grids[threadID][idx(rows+2, i, 0)] = upBufs[neigh[3]][i-1];
						
				//omp_unset_lock(&upLocks[neigh[3]]);
			}

			
			updateGrid(rows, cols, newGrids[threadID], grids[threadID]);
			copybackGrid(rows, cols, newGrids[threadID], grids[threadID]);
		
			//calculate how close the existing answer is to the real answer
			if (workItems[wid]->updates % resCalcFreq == 0)
				workItems[wid]->residual = calcLocalResidual(rows+2, cols+2, grids[wid]);

			#pragma omp single nowait 
			{
				if (workItems[wid]->updates % resCalcFreq == 0) {
					RealNumber tempGlobalNormR = 0;
					for (int i = 0; i < nThreads; i++){
						tempGlobalNormR += workItems[i]->residual;
					}
					globalNormR = sqrt(tempGlobalNormR) / initGlobalNormR;	
					printf("Iteration: %i | Residual ~ %f\n", workItems[threadID]->updates, globalNormR);

                    if (tolerance >= 0 && globalNormR <= tolerance) {
                        shouldContinue = 0;
                    }
				}
			}

			workItems[wid]->updates++;
			#pragma omp flush
			if (maxIters >= 0 && workItems[wid]->updates >= maxIters) {
				shouldContinue = 0;
			}
		}

		#pragma omp single
		{
			duration = omp_get_wtime() - duration;
			printf("Calculation time: %f\n", duration);
		}
	}


/*===Printing and cleanup===*/
	printExperimentOutput(duration, workItems, nThreads, grids, nThreads, cols, rows, initGlobalNormR);
	printGridsToFile(rowsG, colsG, grids, rows+2, cols+2, nThreads, npy);

	return 0;
}
