/*   Copyright 2017 Justs Zarins

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>
#include <sched.h>
#include "precision.h"
#include "ioutils.h"
#include "jacobi.h"
#include "jacobi_omp.h"
#include "pgmio.h"
#include "settings.h"
#include GSL

#define SEED 918237

int omp_jacobi_sync(int npx, int npy, int myN, RealNumber tolerance, int maxIters, int resCalcFreq, WorkItem** workItems) {
	printf("Using synchronous Jacobi implementation.\n");
/*===Initialise variables===*/
	RealNumber scale = 100; // Larger means less diffuse input;
	
	int rowsG, colsG; //G stands for global
	int nThreads = npx * npy;

	int rows = myN; //local  rows
	int cols = myN; //local  cols
	rowsG = rows * npy; //global rows
	colsG = cols * npx; //global cols
	
	//arrays for intermediate reconstruction calculations, created with size +1 in each direction for halos
	RealNumber** grids = malloc(nThreads * sizeof(RealNumber*));
	RealNumber** newGrids = malloc(nThreads * sizeof(RealNumber*));
	RealNumber initVal = 1.0;
	RealNumber globalNormR = 0.0;
	RealNumber initGlobalNormR = 0.0;
	RealNumber globalAvg = -1;

	const gsl_rng* r = gsl_rng_alloc(gsl_rng_mt19937);
	gsl_rng_set(r, SEED); //set seed;

	double duration;

    volatile int shouldContinue = 1;

/*===Start main parallel region===*/
	#pragma omp parallel num_threads(nThreads) default(none) shared(grids, newGrids, rows, cols, npx, npy, colsG, scale, nThreads, rowsG, globalNormR, initVal, tolerance, maxIters, resCalcFreq, duration, r, workItems, initGlobalNormR, shouldContinue) 
	{
		#ifdef INIT_BY_THREAD_ID
		int threadID = omp_get_thread_num();
		#else
		int threadID = sched_getcpu();
		#endif


		#pragma omp critical
			printf("Thread = %d, core = %d\n", threadID, sched_getcpu());

		grids[threadID] = malloc((rows + 2) * (cols + 2) * sizeof(RealNumber));
		newGrids[threadID] = malloc((rows + 2) * (cols + 2) * sizeof(RealNumber));
		
		WorkItem* wi = malloc(sizeof(WorkItem));
		wi->id = threadID;
		wi->pe = sched_getcpu();
		wi->updates = 0;
		wi->residual = -1;

		getCoords(threadID, npy, wi->coords);
		getNeighbours(threadID, npx, npy, wi->coords, wi->neigh);

		workItems[wi->id] = wi;
	
		initialiseGridOMP(rows, cols, colsG, grids[threadID], wi->coords, initVal, scale, NULL);
		setupResidualsSync(rows, cols, grids[threadID], &globalNormR, &initGlobalNormR);

		#pragma omp single nowait
		{
			printf("Running on %d threads.\nGlobal problem size: %d rows by %d columns.\nLocal problem size: %d rows by %d columns.\n ", nThreads, rowsG, colsG, rows, cols);
			printf("sqrt((b-Ax)**2) = %f\n", initGlobalNormR);
			printf("Initial norm r = %f\n", globalNormR);
		}
	
		int iter = 0;
/*===Iterate Jacobi===*/

		#pragma omp single	
			duration = omp_get_wtime();

        while (shouldContinue == 1) {
			int wid = workItems[threadID]->id;
			#pragma omp barrier
			getHalos(rows+2, cols+2, wid, workItems[wid]->neigh, grids);
			#pragma omp barrier

			updateGrid(rows, cols, newGrids[threadID], grids[threadID]);
			copybackGrid(rows, cols, newGrids[threadID], grids[threadID]);
		
			//calculate how close the existing answer is close to the real answer
			if (iter % resCalcFreq == 0) {
				getGlobalNormRSync(rows, cols, grids[threadID], &globalNormR, initGlobalNormR);
				#pragma omp single nowait
					printf("Iteration: %i | Residual: %f\n", iter, globalNormR);
                if (tolerance >= 0 && globalNormR <= tolerance) {
                    shouldContinue = 0;
                }
			}

			(workItems[wid]->updates)++;
            iter++;
            if (maxIters >= 0 && iter >= maxIters) {
                shouldContinue = 0;
            }
		}

		#pragma omp single
		{
			duration = omp_get_wtime() - duration;
		}
	}


/*===Printing and cleanup===*/
	printExperimentOutput(duration, workItems, nThreads, grids, nThreads, cols, rows, initGlobalNormR);
	printGridsToFile(rowsG, colsG, grids, rows+2, cols+2, nThreads, npy);

	return 0;
}
