/*   Copyright 2017 Justs Zarins

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <mpi.h>
#include <sched.h>
#include <unistd.h>

#include "accumulator.h"
#include "jacobi.h"
#include "settings.h"
#include "utils.h"


#define SAMPLING_FREQUENCY 100000 //sleep length between samples in microseconds
#define APPROX_TIME 90 //approximate runtime in seconds
#define SAMPLES (int) (APPROX_TIME * 1000000.0 / SAMPLING_FREQUENCY)


void printHostname(int rank) {
	size_t hlen = 64;
	char hostname[hlen];
	gethostname(hostname, hlen);

	printf("rank %d is on %s\n", rank, hostname);
}


int main(int argc, char *argv[]) {
	MPI_Init(&argc,&argv);
	int rank, worldSize;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &worldSize);

	int jacobiType, npx, npy, myN, resCalcFreq, noiseSize, noiseFreq;
	RealNumber tolerance;
    int maxIters;
	int xsplit, ysplit;
	char* configFile;
    


	//get and check command line input 
	if (argc != 2) {
		printf("usage: %s config_file\n", argv[0]);
		printf("Provided %d arguments.\n", argc);
		for (int i = 1; i < argc; i++)
			printf("%s\n", argv[i]);
		return 1;
	} else {
        configFile = argv[1];
	}
    
    Config* config = parseConfig(configFile);
    printConfig(config);

    jacobiType = config->jacobiType;
    npx = config->npx;
    npy = config->npy;
    myN = config->myN;
    tolerance = config->tolerance;
    maxIters = config->maxIters;
    resCalcFreq = config->resCalcFreq;
    noiseSize = config->noiseSize;
    noiseFreq = config->noiseFreq;
    xsplit = config->xsplit;
    ysplit = config->ysplit;

    if ((tolerance < 0 && maxIters < 0) || (tolerance > 0 && maxIters > 0)) {
        printf("Either tolerance or maxIters has to be less than 0, not both, not neither. Exiting.\n");
        return 1;
    }

    if (jacobiType != 4 && (xsplit != 1 || ysplit != 1)) {
        printf("Should only have subsplitting with jacobi type %d. Resetting subsplits to 1.\n", ASYNC_SUBSPLIT);
	xsplit = 1;
	ysplit = 1;
    }
	
	int nThreads = npx * npy; //TODO: could check for max available threads
	int split = xsplit * ysplit;
	int nWI = nThreads * split;

	//launch jacobi and support workers
	omp_set_nested(1); //enable nesting
	omp_set_dynamic(0);

	int jacobiReturn;
	if (rank != 0) {
		if (config->enableNoise == 1) {
            printHostname(rank);
            printf("Accumulator is on core %d with rank %d.\n", sched_getcpu(), rank);
            accumulate(noiseSize, noiseFreq);
            printf("DONE: Accumulator on core %d with rank %d.\n", sched_getcpu(), rank);
        } else {
            accumulate(0, 100000);
        }
	} else {
		printHostname(rank);
		printf("Normal app is on core %d.\n", sched_getcpu());
		printf("Max number of threads: %d \n", omp_get_max_threads());
		printf("Nesting enabled: %d\n", omp_get_nested());
		volatile int shouldContinue = 1;
		WorkItem** workItems = malloc(nWI * sizeof(WorkItem*));
		int* dirty = calloc(nThreads, sizeof(int));

		Manager* manager = initManager(config);
		manager->ctx->workItems = workItems;
		manager->ctx->nWIs = nWI;
		manager->ctx->dirty = dirty;

        #ifdef SAMPLER
        int num_outer = 2;
        #else
        int num_outer = 1;
        #endif
	
		#pragma omp parallel num_threads(num_outer)
		{
			int threadID = omp_get_thread_num();

			if (threadID == 0) {
				printf("number of threads in outer region: %d\n", omp_get_num_threads());
				
				switch(jacobiType) {
					case SYNC:
						jacobiReturn =  omp_jacobi_sync(npx, npy, myN, tolerance, maxIters, resCalcFreq, workItems);
						break;

					case NEIGH_SYNC:
						jacobiReturn = omp_jacobi_sync_loose(npx, npy, myN, tolerance, resCalcFreq, workItems);
						break;

					case SEMI_SYNC:
						jacobiReturn = omp_jacobi_semi_sync(npx, npy, myN, tolerance, maxIters, resCalcFreq, manager->ctx->maxStaleness, workItems);
						break;

					case ASYNC:
						jacobiReturn = omp_jacobi_racy(npx, npy, myN, tolerance, resCalcFreq, manager);
						break;

					case ASYNC_SUBSPLIT:
						jacobiReturn = omp_jacobi_racy_subsplit(xsplit, ysplit, npx, npy, myN, tolerance, maxIters, resCalcFreq, manager);
						break;

					default:
						printf("Received unknown jacobi type: %d. Exiting.\n", jacobiType);

				}
				//jacobiReturn = omp_jacobi_racy_spread(npx, npy, myN, tolerance, resCalcFreq, managingFreqTime, workItemIters, residuals);

				shouldContinue = 0;
				#pragma omp flush(shouldContinue)
			} else if (threadID == 1) {
				printf("sampler thread here\n");
				#ifdef SAMPLER
				printf("Approximate runtime has been given as %ds. Will take %d thread progress samples.\n", APPROX_TIME, SAMPLES);
				int* samplesWI;
				RealNumber* samplesR;
				int sampleCounter = 0;
				printf("Sampler is on core %d and thread %d\n", sched_getcpu(), threadID);
				samplesWI = malloc(SAMPLES * nWI * sizeof(int));
				samplesR = malloc(SAMPLES * nWI * sizeof(RealNumber));
				#endif //SAMPLER
				

				//this thread periodically wakes up to read some statistics on the execution progress
				while (shouldContinue) {
				#ifdef SAMPLER
					int WIinitialised = 0;
					//check that all work items have been initialised, before sampling them
					while (!WIinitialised) {
						for (int i = 0; i < nWI; i++) {
							if (workItems[i]) {
								WIinitialised = 1;	
							} else {
								WIinitialised = 0;
								break;
							}
						}
					}
					if (sampleCounter < SAMPLES) {
						for (int i = 0; i < nWI; i++) {
							samplesWI[sampleCounter * nWI + i] = workItems[i]->updates;
							samplesR[sampleCounter * nWI + i] = workItems[i]->residual;
						}
						sampleCounter++;
					}
					usleep(SAMPLING_FREQUENCY);
				#else
					usleep(100000); //sampler thread needs to sleep; otherwise it consumes CPU while waiting for parallel region to end
				#endif //SAMPLER
					#pragma omp flush
					//#pragma omp flush(residuals, shouldContinue)
				}
				printf("Sampler on core %d and thread %d has finished.\n", sched_getcpu(), threadID);

				#ifdef SAMPLER
				printf("printing sampling data...\n");
				for (int i = 0; i < sampleCounter; i++) {
					for (int j = 0; j < nWI; j++) {
						printf("%d ", samplesWI[i * nWI + j]);
					}
					printf("\n");
					
					for (int j = 0; j < nWI; j++) {
						printf("%.14f ", samplesR[i * nWI + j]);
					}
					printf("\n");
				}
				printf("finished printing sampling data.\n");
				#endif //SAMPLER
			} else {
				printf("Err, too many threads.\n");
			}
		} //end parallel region
		

        //communicate end-of-program to noise generating ranks
        int message = 1;
        for (int i = 1; i < worldSize; i++)
            MPI_Send(&message, 1, MPI_INT, i, 0, MPI_COMM_WORLD);

	}

	MPI_Finalize();
	printf("Application finishing successfuly.\n");
	return jacobiReturn;
}
