/*   Copyright 2017 Justs Zarins

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <mpi.h>
#include <math.h>
#include "precision.h"
#include "ioutils.h"
#include "jacobi_mpi.h"
#include "jacobi.h"





int main(int argc, char *argv[]) {
	MPI_Init(&argc, &argv);
	//common use status variables for halo swap tracking
	MPI_Request request;
	MPI_Status status;
	
	RealNumber tolerance;
	int resCalcFreq;

	RealNumber scale = 100; // Larger means less diffuse input;
	
	int rowsG, colsG, rank, size;	//G stands for global
	int npx, npy; //number of PE's in x and y directions
	int myN; //local problem size (local domains are square)
	
	MPI_Comm_size(MPI_COMM_WORLD, &size);
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);

	//get and check command line input 
	if (rank == 0) {
		if (argc != 6) {
			printf("usage: %s npx npy myN tolerance resCalcFreq\n", argv[0]);
			printf("Provided %d arguments.\n", argc);
			MPI_Abort(MPI_COMM_WORLD, 1);
		} else {
			npx = atoi(argv[1]);
			npy = atoi(argv[2]);
			myN = atoi(argv[3]);
			tolerance = atof(argv[4]);
			resCalcFreq = atoi(argv[5]);
		}

		if (npx * npy != size) {
			printf("Expected %d PEs, but got %d. Exiting.\n", npx * npy, size);
			MPI_Abort(MPI_COMM_WORLD, 1);
		}
	}

	//broadcast input values
	MPI_Bcast(&npx, 1, MPI_INTEGER, 0, MPI_COMM_WORLD);
	MPI_Bcast(&npy, 1, MPI_INTEGER, 0, MPI_COMM_WORLD);
	MPI_Bcast(&myN, 1, MPI_INTEGER, 0, MPI_COMM_WORLD);
	MPI_Bcast(&tolerance, 1, MPI_REALNUMBER, 0, MPI_COMM_WORLD);
	MPI_Bcast(&resCalcFreq, 1, MPI_INTEGER, 0, MPI_COMM_WORLD);

	int rows = myN; //local  rows
	int cols = myN; //local  cols
	rowsG = rows * npy; //global rows
	colsG = cols * npx; //global cols
	
	//arrays for intermediate reconstruction calculations, created with size +1 in each direction for halos
	RealNumber* grid = malloc((rows + 2) * (cols + 2) * sizeof(RealNumber));
	RealNumber* newGrid = malloc((rows + 2) * (cols + 2) * sizeof(RealNumber));
	
	//create communicatior that represents a 2D cartesian grid
	int dims[2] = {npx, npy};
	int periods[2] = {0, 0};
	MPI_Comm com;
	MPI_Cart_create(MPI_COMM_WORLD, 2, dims, periods, 0, &com);

	//get my rank in the new communicator
	MPI_Comm_rank(com, &rank);

	//calculate ranks of the neighbouring processes
	int leftRank, rightRank, upRank, downRank; 
	MPI_Cart_shift(com, 0, 1, &leftRank, &rightRank);
	MPI_Cart_shift(com, 1, 1, &downRank, &upRank);
	int coords[2];
        MPI_Cart_coords(com, rank, 2, coords);
	//printf("Rank=%d; coords=%d, %d.\n", rank, coords[0], coords[1]);


	//initialize grid to arbitrary value
	RealNumber initVal = 1.0;
	for (int i = 0; i < cols+2; i++) {
		for (int j = 0; j < rows+2; j++) 
			grid[idx(rows+2, i, j)] = initVal;
	}

	RealNumber x = 0.0;
	RealNumber temp = 0.0;
	RealNumber normB = 0.0;
	RealNumber globalNormB = 0.0;
	//boundary conditions, create a bump at the bottom of the box (on last row)
	if (coords[1] == 0) {
		for (int i = 1; i <= cols; i++) {
			x = (i+coords[0]*cols)/(colsG+1.0); 
			temp = exp(-scale*(0.5-x)*(0.5-x));
			grid[idx(rows+2, i, 0)] = initVal + temp;
			normB += temp*temp;
			//printf("%d %d %f %f\n", coords[0], i+coords[0]*Np, x, grid[idx(Np+2, Mp+1, i)]);
		}	
	}

	MPI_Allreduce(&normB, &globalNormB, 1, MPI_REALNUMBER, MPI_SUM, com);
	globalNormB = sqrt(globalNormB);

	if (rank == 0) {
		printf("Running on %d processes.\nGlobal problem size: %d rows by %d columns.\nLocal problem size: %d rows by %d columns.\n ", size, rowsG, colsG, rows, cols);
		printf("Dims: %d, %d.\n", dims[0], dims[1]);
		printf("Norm of b = %f\n", globalNormB);
	}


	//define a vector datatype for up/down halo swaps
	MPI_Datatype horizontalHalo;
	MPI_Type_vector(cols, 1, rows+2, MPI_REALNUMBER, &horizontalHalo); //count, block, stride
	MPI_Type_commit(&horizontalHalo);

	RealNumber globalAvg = -1;
	RealNumber residual = calcResidual(rows+2, cols+2, grid, globalNormB, &com);
	int iter;


	//iterative reconstruction loop
	double startTime = MPI_Wtime();
	for (iter = 0; residual > tolerance; iter++) {
	//for (iter = 0; iter < 10; iter++) {
		swapHalos(rows+2, cols+2, grid, rightRank, leftRank, upRank, downRank, &com, &horizontalHalo, &request, &status);
	
		updateGrid(rows, cols, newGrid, grid);
		copybackGrid(rows, cols, newGrid, grid);
		
		//calculate how close the existing answer is close to the real answer
		if (iter % resCalcFreq == 0) {
			residual = calcResidual(rows+2, cols+2, grid, globalNormB, &com);
			if (rank == 0)
				printf("Iteration: %i | Residual: %f\n", iter, residual);
		}

		
		
	}
	double reconstructionTime = MPI_Wtime() - startTime;
	double totalCPUTime;
	MPI_Allreduce(&reconstructionTime, &totalCPUTime, 1, MPI_DOUBLE, MPI_SUM, com);

	//printf("Rank %i finished reconstruction loop.\n", rank);

	globalAvg = calcAvgPixelVal(rows+2, cols+2, grid, &com, size);
	if (rank == 0) {
		printf("Total CPU Time for reconstruction loop: %f. Per CPU: %f\n", totalCPUTime, totalCPUTime/size);
		printf("Ran for a total of %i iterations. Final global avg and residual: %f, %f.\n", iter, globalAvg, residual);
	}

	
	//printGridToFile(rows, cols, grid, rowsG, colsG, dims, &com); 
	
	free(grid);
	free(newGrid);
	MPI_Finalize();
	return 0;
}
