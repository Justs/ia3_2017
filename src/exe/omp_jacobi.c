/*   Copyright 2017 Justs Zarins

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <math.h>
#include "precision.h"
#include "ioutils.h"
#include "jacobi.h"
#include "jacobi_omp.h"
#include "pgmio.h"
#include GSL

#define SEED 918237

int main(int argc, char *argv[]) {
/*===Initialise variables===*/
	RealNumber tolerance;
	int resCalcFreq;

	RealNumber scale = 100; // Larger means less diffuse input;
	
	int rowsG, colsG, nThreads;	//G stands for global
	int npx, npy; //number of PE's in x and y directions
	int myN; //local problem size (local domains are square)
	
	#pragma omp parallel default(none) shared(nThreads)
		nThreads = omp_get_num_threads();
	
	//get and check command line input 
	if (argc != 6) {
		printf("usage: %s npx npy myN tolerance resCalcFreq\n", argv[0]);
		printf("Provided %d arguments.\n", argc);
		return 1;
	} else {
		npx = atoi(argv[1]);
		npy = atoi(argv[2]);
		myN = atoi(argv[3]);
		tolerance = atof(argv[4]);
		resCalcFreq = atoi(argv[5]);
	}
	if (npx * npy != nThreads) {
		printf("Expected %d threads, but got %d. Exiting.\n", npx * npy, nThreads);
		return 1;
	}

	int rows = myN; //local  rows
	int cols = myN; //local  cols
	rowsG = rows * npy; //global rows
	colsG = cols * npx; //global cols
	
	//arrays for intermediate reconstruction calculations, created with size +1 in each direction for halos
	RealNumber** grids = malloc(nThreads * sizeof(RealNumber*));
	RealNumber** newGrids = malloc(nThreads * sizeof(RealNumber*));
	RealNumber initVal = 1.0;
	RealNumber globalNormB = 0.0;
	RealNumber globalNormR = 0.0;
	RealNumber globalAvg = -1;

	const gsl_rng* r = gsl_rng_alloc(gsl_rng_mt19937);
	gsl_rng_set(r, SEED); //set seed;

	double duration;

/*===Start main parallel region===*/
	#pragma omp parallel default(none) shared(grids, newGrids, rows, cols, npx, npy, colsG, scale, globalNormB, nThreads, rowsG, globalNormR, initVal, tolerance, resCalcFreq, duration, r) 
	{
		int threadID = omp_get_thread_num();
		grids[threadID] = malloc((rows + 2) * (cols + 2) * sizeof(RealNumber));
		newGrids[threadID] = malloc((rows + 2) * (cols + 2) * sizeof(RealNumber));
		
		int* coords = malloc(2 * sizeof(int));
		int* neigh  = malloc(4 * sizeof(int));
		getCoords(threadID, npy, coords);
		getNeighbours(threadID, npx, npy, coords, neigh);
	
		double normB = initialiseGridOMP(rows, cols, colsG, grids[threadID], coords, initVal, scale, &globalNormB, NULL);
		#pragma omp critical(normB_accum)
			globalNormB += normB;
		#pragma omp barrier
		#pragma omp single
			globalNormB = sqrt(globalNormB);

		#pragma omp single nowait
		{
			printf("Running on %d threads.\nGlobal problem size: %d rows by %d columns.\nLocal problem size: %d rows by %d columns.\n ", nThreads, rowsG, colsG, rows, cols);
			printf("Norm of b = %f\n", globalNormB);
		}
	
		getGlobalNormRSync(rows, cols, grids[threadID], &globalNormR, globalNormB);

		#pragma omp single nowait
			printf("Initial norm r = %f\n", globalNormR);
	

		int iter;
/*===Iterate Jacobi===*/

		#pragma omp single	
			duration = omp_get_wtime();

		for (iter = 0; globalNormR > tolerance; iter++) {
			#pragma omp barrier
			getHalos(rows+2, cols+2, threadID, neigh, grids);
			#pragma omp barrier

			updateGrid(rows, cols, newGrids[threadID], grids[threadID]);
			copybackGrid(rows, cols, newGrids[threadID], grids[threadID]);
		
			//calculate how close the existing answer is close to the real answer
			if (iter % resCalcFreq == 0) {
				getGlobalNormRSync(rows, cols, grids[threadID], &globalNormR, globalNormB);
				#pragma omp single nowait
					printf("Iteration: %i | Residual: %f\n", iter, globalNormR);
			}
		}

		#pragma omp single
		{
			duration = omp_get_wtime() - duration;
			printf("Calculation time: %f\n", duration);
			printf("Final Iteration: %i | Residual: %f\n", iter, globalNormR);
		}
	}


/*===Printing and cleanup===*/
	globalAvg = 0.0;
	globalNormR = 0.0;
	for (int g = 0; g < nThreads; g++) {
		for (int i = 1; i < cols+1; i++) {
			for (int j = 1; j < rows+1; j++) {
				globalAvg += grids[g][idx(rows+2, i, j)];
			}
		}
		globalNormR += calcLocalResidual(rows+2, cols+2, grids[g]);
	}
	globalAvg /= rows*cols*nThreads;
	globalNormR = sqrt(globalNormR) / globalNormB;
	printf("Avg cell value: %f Final residual: %f\n", globalAvg, globalNormR);
				

	printGridsToFile(rowsG, colsG, grids, rows+2, cols+2, nThreads, npy);
	//should free grids and newGrids...
}
