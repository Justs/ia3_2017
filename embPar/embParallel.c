/*   Copyright 2017 Justs Zarins

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sched.h>

#define ITEMS 50000
#define REPEATS 150000

#define SAMPLING_FREQUENCY 10000 //sleep length between samples in microseconds
#define APPROX_TIME 35 //approximate runtime in seconds
#define SAMPLES (int) (APPROX_TIME * 1000000.0 / SAMPLING_FREQUENCY)

double doWork(int low, int high);


int main() {
	int nThreads = 1;
	#pragma omp parallel
	{
		nThreads = omp_get_num_threads();
	}

	int* iters = calloc(nThreads, sizeof(int));
	int* samples;
	int sampleCounter = 0;
	int nThreadsFinished = 0;
	double startTime, duration;

	printf("Approximate runtime has been given as %ds. Will take %d thread progress samples.\n", APPROX_TIME, SAMPLES);

	#pragma omp parallel default(shared)
	{
		int id = omp_get_thread_num();
		printf("Thread %d is on core %d\n", id, sched_getcpu());
		
		
		if (id == nThreads-1)
			samples = malloc(SAMPLES * nThreads * sizeof(int));

		#pragma omp master
			startTime = omp_get_wtime();
		#pragma omp barrier
		if (id == nThreads-1) { //monitor thread
			printf("Sampler is on core %d and thread %d\n", sched_getcpu(), id);
			while (nThreadsFinished != nThreads-1) {
				if (sampleCounter < SAMPLES) {
					#pragma omp flush
					for (int i = 0; i < nThreads; i++)
						samples[sampleCounter * nThreads + i] = iters[i];
					sampleCounter++;
				}
				//printf("Sampler is on core %d\n", sched_getcpu());
				usleep(SAMPLING_FREQUENCY);
				
			}
		} else { //worker threads
			double sum = 0;
			for (int i = 0; i < REPEATS; i++) {
				sum += doWork(0+id, ITEMS+id);
				iters[id]++;
				#pragma omp flush
				//if (id == 0) printf("%d\n", iters[id]);
			}
			#pragma omp atomic
				nThreadsFinished++;
			while (nThreadsFinished != nThreads-1) { //enter a spin loop when done.
				#pragma omp flush(nThreadsFinished)
			}
			printf("%d\'s sum is %f %d\n", id, sum, iters[id]); 	
		}
		#pragma omp master
			duration = omp_get_wtime() - startTime;
	}


	printf("Run took %f s\n", duration);
	printf("printing sampling data...\n");
	for (int i = 0; i < sampleCounter; i++) {
		for (int j = 0; j < nThreads - 1; j++) {
			printf("%d ", samples[i * nThreads + j]);
		}
		printf("\n");
	}
	printf("finished printing sampling data.\n");

}


double doWork(int low, int high) {
	double sum = 0;
	for (int i = low; i < high; i++) {
		sum += i * 1.1;
	}
	return sum;
}
