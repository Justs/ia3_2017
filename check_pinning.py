#   Copyright 2017 Justs Zarins
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#!/usr/bin/python
import sys
import os
import re


if __name__ == "__main__":
    argc = len(sys.argv)
    if argc != 2:
        print "usage: python2 " + sys.argv[0] + " EXPERIMENT_OUTPUT_FILE" 
        sys.exit()

    filename = sys.argv[1]

    appNodeID = ""
    noise_core_nodes = {}
    noisy_cores = {}
    worker_threads = {}
    with open(filename, 'r') as f:
        for line in f:
            res = re.search("Accumulator is on core (\d+) with threadID (\d+)\.", line)
            if res:
                noisy_cores[int(res.group(2))] = {"init_core": int(res.group(1))}
                continue
            
            res = re.search("Accumulator is on core (\d+) with rank (\d+)\.", line)
            if res:
                noisy_cores[int(res.group(2))] = {"init_core": int(res.group(1))}
                continue
            
            res = re.search("DONE: Accumulator on core (\d+) with threadID (\d+)\.", line)
            if res:
                noisy_cores[int(res.group(2))]["final_core"] = int(res.group(1))
                continue
            
            res = re.search("DONE: Accumulator on core (\d+) with rank (\d+)\.", line)
            if res:
                noisy_cores[int(res.group(2))]["final_core"] = int(res.group(1))
                continue


            res = re.search("Init: Thread = (\d+), core = (\d+)\.", line)
            if res:
                worker_threads[int(res.group(1))] = {"init_core": int(res.group(2))}
                continue
            
            res = re.search("Final: Thread = (\d+), core = (\d+)\.", line)
            if res:
                worker_threads[int(res.group(1))]["final_core"] = int(res.group(2))
                continue

            res = re.search("rank (\d+) is on (.+)", line)
            if res:
                num = int(res.group(1))
                nodeID = res.group(2)

                if num == 0:
                    appNodeID = nodeID
                else:
                    noise_core_nodes[num] = nodeID



    if len(noisy_cores) == 0:
        print "No noise generator found."
    else:
        for k,v in noisy_cores.iteritems():
            if (v["init_core"] == v["final_core"]):
                print "Found noise pinned to core", v["init_core"]
            else:
                print "Noise generator with rank/thread", k, "pinned incorrectly. It moved from core", v["init_core"], "to", v["final_core"], "during the run."

        for k,v in noise_core_nodes.iteritems():
            if v != appNodeID:
                print "Noise generator with rank/thread", k, "pinned incorrectly. It is pinned to a different node(", v, ") than the main application which is on node", appNodeID, "."

    

    if len(worker_threads) == 0:
        print "No threads found."
    else:
        threads_ok = True
        for k,v in worker_threads.iteritems():
            if (k != v["init_core"]):
                print "Worker thread", k, "not pinned to core with same ID but instead to core", v["init_core"]
                threads_ok = False

            if (v["init_core"] != v["final_core"]):
                print "Worker thread", k, "pinned incorrectly. Moved from core", v["init_core"], "to", v["final_core"]
                threads_ok = False

        if threads_ok:
            print "Worker threads pinned correctly."
