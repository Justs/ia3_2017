#include "thread_manager.h"

/*Used to compare reals to sort in ascending order.*/
int compareReals (const void * a, const void * b);

/*Prints out the min, median, mean, max and standard deviation of a list of real numbers.*/
void printListStats(RealNumber* list, int n);


typedef struct Config {
    int jacobiType;
    int npx;
    int npy;
    int myN;
    double tolerance;
    int maxIters;
    int resCalcFreq;
    int enableNoise;
    int noiseSize;
    int noiseFreq;
    int xsplit;
    int ysplit;
	
    int managerType;
    double managingFreqTime;
    int nPairs;
	int lowerThresh;
	int upperThresh;
	int maxStaleness;
	int splits;
	int crossSocketFreq;
	int numCores;
} Config;

Config* parseConfig(char* filename);
void printConfig(Config* c);

Manager* initManager(Config* c);

void printDefinedVars();
