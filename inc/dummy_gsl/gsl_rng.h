/*   Copyright 2017 Justs Zarins

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

#ifndef GSL_DUMMY_H
#define GSL_DUMMY_H
typedef struct gsl_rng {
	int dummy;
} gsl_rng;

#define gsl_rng_mt19937 0

static inline void doNothing() {
	printf("Using dummy GSL library!\n");
}

static inline unsigned long int gsl_rng_uniform_int(const gsl_rng* r, unsigned long int n) {
	doNothing();
	return 0;
}

static inline double gsl_rng_uniform(const gsl_rng* r) {
	doNothing();
	return 0;
}

static inline gsl_rng* gsl_rng_alloc(int t) {
	doNothing();
	return 0;
}

static inline void gsl_rng_set(const gsl_rng* r, unsigned long int s) {
	doNothing();
}

#endif // GSL_DUMMY_H
