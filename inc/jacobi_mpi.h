#include <mpi.h>
#include "precision.h"

void swapHalos(int rows, int cols, RealNumber* old, int rightRank, int leftRank, int upRank, int downRank, MPI_Comm *com, MPI_Datatype *horizontalHalo, MPI_Request *request, MPI_Status *status);

RealNumber calcAvgPixelVal(int rows, int cols, RealNumber* grid, MPI_Comm *com, int size);

RealNumber calcResidual(int rows, int cols, RealNumber* grid, RealNumber globalNormB, MPI_Comm *com);

void printGridToFile(int rows, int cols, RealNumber* grid, int rowsG, int colsG, int* dims, MPI_Comm* com);
