#include "precision.h"
#include "jacobi.h"
#include GSL


void getGlobalNormRSync(int rows, int cols, RealNumber* grid, RealNumber* globalNormR, RealNumber normaliser);

RealNumber initialiseGridOMP(int rows, int cols, int colsG, RealNumber* grid, int* coords, RealNumber initVal, RealNumber scale, const gsl_rng* r);

RealNumber setupResidualsSync(int rows, int cols, RealNumber* grid, RealNumber* globalNormR, RealNumber* initGlobalNormR);

void subsplitInit(int rows, int cols, int colsG, RealNumber** grids, RealNumber initVal, RealNumber scale, const gsl_rng* r, WorkItem** workItems, int nWIS);
