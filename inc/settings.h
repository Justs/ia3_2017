//if defined, some sort of thread managing will be done
#define MANAGE 

//if defined, information about moving of threads will be printed
//#define MANAGER_OUTPUT 

//if defined, the extra thread will gather samples periodically; otherwise it will just exit
//#define SAMPLER 

//if defined, will output summary of iteration stats etc.
//#define SHORT_AGGREGATE_OUTPUT 

// if defined, will output all final data (to be then dealt with in a separate script)
#define DUMP_AGGREGATE_OUTPUT 

// if defined, problem domains will be initialised according to thread id. If not defined,
// core id will be used. The latter may be useful if thread binding isn't working correctly.
#define INIT_BY_THREAD_ID

//depreceated
#define MAX_ITERS 0
