// links to dummy GSL library to remove it as a dependency when not needed
#define GSL <dummy_gsl/gsl_rng.h>

//if defined, the final grid will not be printed to file
#define DONT_PRINT_GRID

#include "precision.h"
#include "thread_manager.h"
#include "workItem.h"
#include GSL
#include <omp.h>
#define idx(rows,x,y) (x)*(rows) + (y)

RealNumber calcLocalResidual(int rows, int cols, RealNumber* grid);

void getHalos(int rows, int cols, int me, int* neigh, RealNumber** grids);

void updateGrid(int rows, int cols, RealNumber* newGrid, RealNumber* grid);

void updateGridPartially(int rows, int cols, RealNumber* newGrid, RealNumber* grid, RealNumber skipPercentage, const gsl_rng* r);

void copybackGrid(int rows, int cols, RealNumber* newGrid, RealNumber* grid);

void printGridsToFile(int rowsG, int colsG, RealNumber** grids, int rows, int cols, int n, int npy);

void getCoords(int n, int npy, int* coords);

void getNeighbours(int n, int npx, int npy, int* coords, int* neighbours);

//enum {SYNC, NEIGH_SYNC, SEMI_SYNC, ASYNC, ASYNC_SUBSPLIT};
#define SYNC 0
#define NEIGH_SYNC 1
#define SEMI_SYNC 2
#define ASYNC 3
#define ASYNC_SUBSPLIT 4



int omp_jacobi_sync(int npx, int npy, int myN, RealNumber tolerance, int maxIters, int resCalcFreq, WorkItem** workItems);
int omp_jacobi_sync_loose(int npx, int npy, int myN, RealNumber tolerance, int resCalcFreq, WorkItem** workItems);
int omp_jacobi_semi_sync(int npx, int npy, int myN, RealNumber tolerance, int maxIters, int resCalcFreq, int maxStaleness, WorkItem** workItems);
int omp_jacobi_racy(int npx, int npy, int myN, RealNumber tolerance, int resCalcFreq, Manager* manager);
int omp_jacobi_racy_subsplit(int xsplit, int ysplit, int npx, int npy, int myN, RealNumber tolerance, int maxIters, int resCalcFreq, Manager* manager);


//int omp_jacobi_racy_spread(int npx, int npy, int myN, RealNumber tolerance, int resCalcFreq, double managingFreqTime, int* workItemIters, RealNumber* residuals);

