#include <omp.h>
#include "precision.h"

#ifndef WORKITEM_H
#define WORKITEM_H

typedef struct WorkItem {
	int id; //work item id
	int pe; //PE currently resonsible for this work item
	int updates; //number of updates completed on this work item
	int coords[2];
	int neigh[4];
	RealNumber residual;
	omp_lock_t lock; //to protect against multiple threads updating the work item at the same time
} WorkItem;

#endif /* WORKITEM_H */
