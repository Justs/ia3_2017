#include "workItem.h"
#pragma once

typedef struct ManagerContext {
	int nThreads;
	int* iters;
	int* workIDs;
	
	WorkItem** workItems;
	int nWIs;
	int* dirty;

	int nPairs;
	int lowerThresh;
	int upperThresh;
	int maxStaleness;
	int splits;
	int crossSocketFreq;
	int crossSocketCounter;
	int numCores;
} ManagerContext;


typedef struct Manager {
	void (*manage)(ManagerContext*);
	double managingFreqTime;
	ManagerContext* ctx;
} Manager;


typedef struct TwoInts {
	int iters;
	int id;
} TwoInts;


void manageWorkItems(ManagerContext* ctx);
void manageWorkItemsGlobal(ManagerContext* ctx);
void manageSplitWorkItems(ManagerContext* ctx);
void manageSplitWorkItems2(ManagerContext* ctx);
void manageSplitWorkItems3(ManagerContext* ctx);
